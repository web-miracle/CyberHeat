import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { domain, uselocalStorage } from '../options';
import { ILogo, IMenuItem, IMenuStore } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

/**
 *   Сервис, в котором описаны методы для получения глобальных настроек сайта
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2018-01-24
 */
@Injectable()
export class GlobalConfigService {

	/**
	 *   При построение используется инекция зависивости от http класса @angular,
	 *   для получения данных по http сервису, подробнее:
	 *   https://angular.io/guide/dependency-injection-in-action
	 *   https://angular.io/guide/http
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 *   @param   Http         private http   http-сервис от @angular
	 */
	constructor(private http: Http) {}

	/**
	 *   Метод для получения данных о логотипе сайта, изначально функция проверяет
	 *   данные о логотипе в хранилеше localStorage, если данные являются актуальными
	 *   на данный момент ( сутки от последнего обновления ), инче данные возвращаются
	 *   по REST API и создается новое значение в хранилище. Возвращает поток
	 *   с объектом картинки Observable<Logo>, подробнее:
	 *   https://caniuse.com/#search=localStorage
	 *   https://developer.mozilla.org/ru/docs/Web/API/Window/localStorage
	 *   http://reactivex.io/rxjs/class/es6/Observable.js~Observable.html#static-method-of
	 *   @autor   Mishalov_Pavel
	 *   @version [version]
	 *   @date    2018-01-24
	 *   @return  {Observable<Logo>} Поток данных, ожидается получить объект Logo
	 */
	getLogo(): Observable<ILogo> {
		const logotype = localStorage.getItem('logotype') || undefined;
		if (logotype && uselocalStorage) {
			const logo: ILogo = JSON.parse(localStorage.getItem('logotype'));
			const nowDate = new Date();
			if ((nowDate.getTime() - logo.time) < (100 * 60 * 60 * 24)) {
				return Observable.of(logo);
			} else {
				localStorage.removeItem('logotype');
			}
		}
		return this.http
					.get(domain + '/wp-json/acf/v3/options/option/logo')
					.map((res) => {
						return res.json();
					})
					.map((answer) => {
						return answer.logo;
					})
					.map((answer) => {
							const nowDate = new Date();
							const logo: ILogo = {
								'time': nowDate.getTime(),
								'url': answer.url,
								'alt': answer.alt,
								'title': answer.title
							};
							localStorage.setItem('logotype', JSON.stringify(logo));
							return logo;
					});
	}

	/**
	 *   Метод для получения данных о навигации, изначально функция проверяет
	 *   данные о меню в хранилеше localStorage, если данные являются актуальными
	 *   на данный момент ( сутки от последнего обновления ), инче данные возвращаются
	 *   по REST API и создается новое значение в хранилище. Возвращает поток
	 *   с массивом из элементов MenuItem Observable<Array<MenuItem>>, подробнее:
	 *   https://caniuse.com/#search=localStorage
	 *   https://developer.mozilla.org/ru/docs/Web/API/Window/localStorage
	 *   http://reactivex.io/rxjs/class/es6/Observable.js~Observable.html#static-method-of
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 *   @return  {Observable}   Поток данных, ожидается получить массив из объектов MenuItem
	 */
	getMenu(): Observable<Array<IMenuItem>> {
		const store = localStorage.getItem('menu') || undefined;
		if (store && uselocalStorage) {
			const menuStore: IMenuStore = JSON.parse(store);
			const nowDate = new Date();
			if ((nowDate.getTime() - menuStore.time) < (100 * 60 * 60 * 24)) {
				return Observable.of(menuStore.data);
			} else {
				localStorage.removeItem('menu');
			}
		}
		return this.http
					.get(domain + '/wp-json/miracle/api/menu')
					.map((res: any) => {
						return res.json();
					})
					.map((res: Array<IMenuItem>) => {
							res.forEach((item: IMenuItem) => {
								if (item.menu_item_parent === '0') {
									item.parent = false;
									item.child = false;
									item.childrens = [];
									res.forEach((childs: IMenuItem) => {
										if (childs.menu_item_parent === item.ID.toString()) {
											item.parent = true;
											item.childrens.push(childs);
										}
									});
								} else {
									item.child = true;
								}
							});
							const nowDate = new Date();
							localStorage.setItem(
								'menu',
								JSON.stringify(
									{
										'time': nowDate.getTime(),
										'data': res
									}
								)
							);
							return res;
						});
	}

}
