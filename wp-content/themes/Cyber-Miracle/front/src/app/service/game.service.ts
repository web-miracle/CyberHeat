import { Injectable } from '@angular/core';
import { IMatch, IMatchMember } from '../interface/interface';

/**
 *   Класс, в котором описаны методы для получения информации о матчах
 *   @autor   Mishalov_Pavel
 *   @version 0.1
 *   @date    2018-01-24
 */
@Injectable()
export class GameService {

	games: Array<IMatch>;

	constructor() {
		this.games = [
			{
				'name': 'StarLadder',
				'gameSlug': 'dota2',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'dota2',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 0
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'dota2',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'csgo',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'csgo',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'csgo',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'csgo',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'csgo',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'dota2',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			},
			{
				'name': 'StarLadder',
				'gameSlug': 'dota2',
				'tourSlug': 'starladder-tour',
				'commands': [
						{
							'name': 'Navi',
							'status': 1
						},
						{
							'name': 'VirtusPro',
							'status': 3
						}
					],
				'online': false,
				'result': 'Завершен'
			}
		];
	}

	getLastGame() {
		return this.games;
	}
}
