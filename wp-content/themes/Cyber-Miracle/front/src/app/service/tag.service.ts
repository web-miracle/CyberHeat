import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ITag } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import { domain, uselocalStorage } from '../options';
import 'rxjs/add/operator/map';

@Injectable()
export class TagService {

	constructor(private http: Http) {}

	getTagByslug(slug: string = ''): Observable<Array<ITag>> {
		const url: string = domain + '/wp-json/wp/v2/tags';
		const params: string = '?slug=' + slug;

		return this.http
					.get(url + params)
					.map((res) => res.json());
	}

}
