import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { IPost } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { domain } from '../options';

/**
 *   Сервис, в котором описаны методы для получения информации о постах
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2018-01-24
 */
@Injectable()
export class PostService {

	constructor(private http: Http) {}

	/**
	 *   Получает поток Observable массива постов за сегодняшний день
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-27
	 *   @return  {Observable<Array<IPost>>}
	 */
	getDatePosts(before: number = 0): Observable<Array<IPost>> {
		const dataBefore = new Date();
		const dataAfter = new Date();
		const dayAfter = dataAfter.getDate() - before;

		dataAfter.setDate(dayAfter);

		const dayBefore = dataBefore.getDate() - before + 1;
		dataBefore.setDate(dayBefore);

		return this.http
			.get(domain + '/wp-json/wp/v2/posts?after=' + dataAfter.toISOString() + '&before=' + dataBefore.toISOString())
			.map((res) => res.json());
	}

	/**
	 * [getPosts description]
	 * @description [получает архивные новости]
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-01-31
	 * @version     [0.1]
	 * @return      {Observable}         [массив новостей]
	 */
	getPosts(): Observable<Array<IPost>> {
		return this.http
			.get(domain + '/wp-json/wp/v2/posts')
			.map((res) => res.json());
	}


	getCategoryPosts(cat: number = 0): Observable<Array<IPost>> {
		return this.http
			.get(domain + '/wp-json/wp/v2/posts?categories=' + cat)
			.map((res) => res.json());
	}


	getTagPosts(tag: number = 0): Observable<Array<IPost>> {
		return this.http
			.get(domain + '/wp-json/wp/v2/posts?tags=' + tag)
			.map((res) => res.json());
	}

	/**
	 *   Получает поток Observable массива последних шести горячих постов
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-27
	 *   @return  {Observable<Array<Object>>}
	 */
	getPrimaryPosts(): Observable<Array<Object>> {
		return this.http
					.get(domain + '/wp-json/miracle/api/primary-post')
					.map((res) => res.json());
	}

	/**
	 *   Получает поток Observable массива 3 рандомных постов
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @return  {Observable<Array<Object>>}
	 */
	getRandomPosts(): Observable<Array<Object>> {
		return this.http
					.get(domain + '/wp-json/miracle/api/rand-posts')
					.map((res) => res.json());
	}

	getPostBySlug(slug: string) {
		return this.http
					.get(domain + '/wp-json/wp/v2/posts?slug=' + slug)
					.map((res) => res.json());
	}

	/**
	 *   Получает посты по переданым параметрам
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-12
	 *   @param   {Observable<Array<IPost>>} Поток с массивом постов
	 */
	getPostQuery(query: string = ''): Observable<Array<IPost>> {
		return this.http
					.get(domain + '/wp-json/wp/v2/posts' + query)
					.map((res) => res.json());
	}
}
