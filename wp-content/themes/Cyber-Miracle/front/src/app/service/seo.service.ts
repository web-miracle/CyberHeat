import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { domain } from '../options';
import { ISeo } from '../interface/interface';

@Injectable()
export class SeoService {

	constructor(
		private title: Title,
		private meta: Meta
	) {}


	setSEOTags(seo: ISeo, url: string = domain + '/', status: string = '200'): void {
		this.clearSEO(status);

		const title: string = seo['seo-title'] || 'CyberHeat';
		this.title
			.setTitle(seo['seo-title']);

		const description: string = seo['seo-description'] || 'Сайт новостей о киберспорте';
		this.meta
			.updateTag({
				'name': 'description',
				'content': description
			});

		const keywords: string|boolean = seo['seo-keywords'] || false;
		if (keywords && typeof keywords === 'string') {
			this.meta
				.updateTag({
					'name': 'keywords',
					'content': keywords
				});
		}

		this.meta
			.updateTag({
				'property': 'og:url',
				'content': url
			});

		this.meta
			.updateTag({
				'property': 'og:type',
				'content': 'website'
			});

		this.meta
			.updateTag({
				'property': 'og:site_name',
				'content': 'CyberHeat'
			});

		const ogTitle: string|boolean = seo['seo-og-title'] || false;
		if (ogTitle && typeof ogTitle === 'string') {
			this.meta
				.updateTag({
					'property': 'og:title',
					'content': ogTitle
				});
		}

		const ogDescription: string|boolean = seo['seo-og-description'] || false;
		if (ogDescription && typeof ogDescription === 'string') {
			this.meta
				.updateTag({
					'property': 'og:description',
					'content': ogDescription
				});
		}

		const ogImage: string|boolean = seo['seo-og-image'] || false;
		if (ogImage && typeof ogImage === 'string') {
			this.meta
				.updateTag({
					'property': 'og:image',
					'content': ogImage
				});
		}

		this.meta
			.updateTag({
				'name': 'twitter:url',
				'content': url
			});

		const twitterTitle: string|boolean = seo['seo-twitter-title'] || false;
		if (twitterTitle && typeof twitterTitle === 'string') {
			this.meta
				.updateTag({
					'name': 'twitter:title',
					'content': twitterTitle
				});
		}

		const twitterDescription: string|boolean = seo['seo-twitter-description'] || false;
		if (twitterDescription && typeof twitterDescription === 'string') {
			this.meta
				.updateTag({
					'name': 'twitter:description',
					'content': twitterDescription
				});
		}

		const twitterImage: string|boolean = seo['seo-twitter-image'] || false;
		if (twitterImage && typeof twitterImage === 'string') {
			this.meta
				.updateTag({
					'name': 'twitter:image',
					'content': twitterImage
				});
		}

		for (let i = seo['seo-meta'].length - 1; i >= 0; i--) {
			this.meta
				.updateTag(
					seo['seo-meta'][i]
				);
		}
	}

	private clearSEO(status): void {
		this.meta.removeTag('name="description"');
		this.meta.removeTag('name="keywords"');
		this.meta.removeTag('name="twitter:url"');
		this.meta.removeTag('name="twitter:title"');
		this.meta.removeTag('name="twitter:description"');
		this.meta.removeTag('name="twitter:image"');
		this.meta.removeTag('property="og:url"');
		this.meta.removeTag('property="og:title"');
		this.meta.removeTag('property="og:description"');
		this.meta.removeTag('property="og:image"');

		this.meta
			.updateTag({
				'name': 'prerender-status-code',
				'content': status
			});
	}

}
