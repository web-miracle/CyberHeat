import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { IStreams } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { domain, uselocalStorage } from '../options';

@Injectable()
export class StreamService {

	options: Object;
	headers: Headers;

	constructor(private http: Http) {
		this.headers = new Headers();
		this.headers.set('Content-Type', 'application/json');
		this.headers.set('Accept', 'application/vnd.twitchtv.v5+json');
		this.headers.set('Client-ID', '0vosrgp8ysse6in0akwicn3q020ul2');

		this.options = {
						'Accept': 'application/vnd.twitchtv.v5+json',
						'Client-ID': '0vosrgp8ysse6in0akwicn3q020ul2'
						};
	}

	getStream(): Observable<IStreams> {
		return this.http.get(domain + '/wp-json/miracle/api/online-streams')
						.map((res) => res.json())
						.mergeMap((res: Array<number>) => {
							const channel = res.join(',');
							return this.http
										.get('https://api.twitch.tv/kraken/streams?channel=' + channel, { 'headers': this.headers });
						})
						.map((res) => res.json());
	}

}
