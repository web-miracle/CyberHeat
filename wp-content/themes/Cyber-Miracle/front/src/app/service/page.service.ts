import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { IPage } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { domain, uselocalStorage } from '../options';

/**
 *   Сервис, в котором описаны методы для получения информации о страницах
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2018-01-24
 */
@Injectable()
export class PageService {

	constructor(private http: Http) {}

	/**
	 * Метод для получение информации о страници по переданому слагу
	 * @date        2018-02-08
	 * @version     1.0
	 * @return      {Observable}      Поток с объектом страници
	 */
	getPage(slug: string): Observable<IPage> {
		const url: string = domain + '/wp-json/wp/v2/pages';
		const params: string = '?slug=' + slug;

		return this.http
					.get(url + params)
					.map((res) => res.json())
					.map((pages: Array<IPage>) => {
						return pages[0];
					});
	}
}
