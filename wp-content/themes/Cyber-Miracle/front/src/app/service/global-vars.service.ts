import { Injectable } from '@angular/core';
import { IUser } from '../interface/interface';
import { RequestOptionsArgs, Headers } from '@angular/http';

@Injectable()
class LoadPage {

	loadPage: string;

	constructor() {
		this.loadPage = 'true';
	}

	set(page: string) {
		this.loadPage = page;
	}

	get() {
		return this.loadPage;
	}
}

@Injectable()
class LoggedInUserService {

	User: IUser = null;

	constructor() { }

	set(user: IUser) {
		this.User = user;
	}

	get() {
		return this.User;
	}

	getAuthHeader(params: Object): RequestOptionsArgs {
		const oauth: string = 'Basic ' + localStorage.getItem('authKeys');
		const header: Headers = new Headers({
									'Content-Type':  'application/json',
									'Authorization': oauth
								});
		const options: RequestOptionsArgs = {
											'headers': header,
											'params': params
										};
		return options;
	}

}

export { LoggedInUserService, LoadPage };
