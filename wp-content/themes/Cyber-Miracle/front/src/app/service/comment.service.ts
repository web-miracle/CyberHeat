import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { IComment, IUser } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import { domain, uselocalStorage } from '../options';
import { LoggedInUserService } from './global-vars.service';
import 'rxjs/add/operator/map';

@Injectable()
export class CommentService {

	constructor(
		private http: Http,
		private loggedUser: LoggedInUserService
	) {}

	getComment(
		postID: number = 0,
		page: number = 1
	): Observable<Array<IComment>> {
		const url: string = domain + '/wp-json/wp/v2/comments';
		const params: string = '?post=' + postID + '&per_page=100&page=' + page;
		const query$: Observable<Array<IComment>> = this.http
														.get(url + params)
														.map((res) => res.json());
		const allComments$: Observable<Array<IComment>> =
				query$.map((comments: Array<IComment>) => {
						if (comments.length === 100) {
							this.getComment(postID, page + 1)
								.subscribe((nextComments: Array<IComment>) => {
									comments = comments.concat(nextComments);
									return comments;
								});
						} else {
							return comments;
						}
					});

		return allComments$;
	}

	getComments(
		postID: number = 0,
		parentComment: number = 0,
		dateISO: string|boolean = false
	): Observable<Array<Comment>> {
		const after: string = dateISO ? '&after=' + dateISO : '';
		const url: string = domain + '/wp-json/wp/v2/comments';
		const params: string = '?post=' + postID + '&parent=' + parentComment + after;

		return this.http
					.get(url + params)
					.map((res) => res.json());
	}

	getCommentQuery(query: string = ''): Observable<Array<IComment>> {
		const url: string = domain + '/wp-json/wp/v2/comments';
		return this.http
					.get(url + '?' + query)
					.map((res) => res.json());
	}

	setComment(
		content: string,
		post: number,
		parent: number = 0
	): Observable<Array<IComment>> {
		const authUser: IUser = this.loggedUser.get();
		const userID: number = authUser.id;
		const header = this.loggedUser.getAuthHeader({ 'content': content });
		const url: string = domain + '/wp-json/wp/v2/comments';
		const params: string = '?post=' + post + '&author=' + userID + '&parent=' + parent;

		return this.http
					.post(url + params, null, header)
					.map((res) => res.json());
	}

}
