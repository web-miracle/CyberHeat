import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ITag } from '../interface/interface';
import { domain, uselocalStorage } from '../options';
import 'rxjs/add/operator/map';

@Injectable()
export class AcfService {

	constructor(private http: Http) {}

	getTagACF(tagID: number = 0): Observable<ITag> {
		const url: string = domain + '/wp-json/acf/v3/tags/' + tagID;

		return this.http
					.get(url)
					.map((res) => res.json());
	}

}
