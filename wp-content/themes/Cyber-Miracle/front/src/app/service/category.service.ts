import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ICategory } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import { domain, uselocalStorage } from '../options';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoryService {

	constructor(private http: Http) {}

	getCategoryByslug(slug: string = ''): Observable<Array<ICategory>> {
		const url: string = domain + '/wp-json/wp/v2/categories';
		const params: string = '?slug=' + slug;

		return this.http
					.get(url + params)
					.map((res) => res.json());
	}

}
