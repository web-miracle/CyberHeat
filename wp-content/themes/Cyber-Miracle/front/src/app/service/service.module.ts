import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalConfigService } from './global-config.service';
import { GameService } from './game.service';
import { PostService } from './posts.service';
import { StreamService } from './stream.service';
import { UserService } from './user.service';
import { CommentService } from './comment.service';
import { LoggedInUserService, LoadPage } from './global-vars.service';
import { CategoryService } from './category.service';
import { TagService } from './tag.service';
import { ScrollToService } from './scrollTo.service';
import { PageService } from './page.service';
import { AcfService } from './acf.service';
import { SeoService } from './seo.service';

/**
 *   Модуль, в котором собираются все сервисы проекта
 */
@NgModule({
	'imports': [
		CommonModule
	],
	'providers': [
		GlobalConfigService,
		GameService,
		PostService,
		StreamService,
		UserService,
		CommentService,
		LoggedInUserService,
		LoadPage,
		CategoryService,
		TagService,
		ScrollToService,
		PageService,
		AcfService,
		SeoService
	]
})
export class ServiceModule {}
