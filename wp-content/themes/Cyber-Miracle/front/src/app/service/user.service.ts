import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { IUser } from '../interface/interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { domain, uselocalStorage } from '../options';
import 'rxjs/add/operator/timeout';

@Injectable()
export class UserService {

	constructor(private http: Http) {}

	getUser(id: number|boolean = false): Observable<IUser> {
		const userId: string = id ? id.toString() : '';
		return this.http
					.get(domain + '/wp-json/wp/v2/users/' + userId)
					.map((res) => res.json());
	}

	signUpUser(login: string, password: string, email: string): Observable<IUser> {
		const url: string = domain + '/wp-json/wp/v2/sign-up/user/';
		const params: string = '?username='	+ login + '&password=' + password + '&email=' + email;
		return this.http
					.post(url + params, '')
					.map((res) => res.json());
	}
}
