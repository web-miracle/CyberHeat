import { MetaDefinition } from '@angular/platform-browser';

/**
 *   В данном файле описано структура классов, их свойства и типы
 */

/**
 *   Объект логотипа сайта, time свойство описывающее время
 *   на который переуд данные актуальны
 */
interface ILogo {
	'time': number;
	'url': string;
	'alt': string;
	'title': string;
}

/**
 *   Объект, который хранится в localStore['menu'], где time момент
 *   на который данные актуальны, data информация о меню
 */
interface IMenuStore {
	'time': number;
	'data': Array<IMenuItem>;
}

/**
 *   Объект описывающей пункт меню, помимо полученых данных с сервера,
 *   свойство parent описывает, есть ли у пункта меню дочерние пункты
 *   child описывает, является ли данный пункт меню дочерним,
 *   childrens массив описывающий все дочерние пункты меню данного
 */
interface IMenuItem {
	'ID': number;
	'post_author': string;
	'post_date': string;
	'post_date_gmt': string;
	'post_content': string;
	'post_title': string;
	'post_excerpt': string;
	'post_status': string;
	'comment_status': string;
	'ping_status': string;
	'post_password': string;
	'post_name': string;
	'to_ping': string;
	'pinged': string;
	'post_modified': string;
	'post_modified_gmt': string;
	'post_content_filtered': string;
	'post_parent': number;
	'guid': string;
	'menu_order': number;
	'post_type': string;
	'post_mime_type': string;
	'comment_count': string;
	'filter': string;
	'db_id': number;
	'menu_item_parent': string;
	'object_id': string;
	'object': string;
	'type': string;
	'type_label': string;
	'title': string;
	'url': string;
	'target': string;
	'attr_title': string;
	'description': string;
	'classes': Array<string>;
	'xfn': string;
	'parent': boolean;
	'child': boolean;
	'childrens': Array<IMenuItem>;
}

/**
 *   Объект матча турнира,
 *   name: Название турнира
 *   gameSlug: Слуг, по которому планируется составлять ссылку на матч
 *   tourSlug: Слуг, по которому планируется составлять ссылку на турнир
 *   commands: Массив из команд учасников матча MatchMember
 *   online: Отображает, проводится ли игра в данный момент
 *   result: Статус матча
 */
interface IMatch {
	'name': string;
	'gameSlug': string;
	'tourSlug': string;
	'commands': Array<IMatchMember>;
	'online': boolean;
	'result': string;
}

/**
 *   Объект участника матча,
 *   name: Название команда
 *   status: Количество побед команды
 */
interface IMatchMember {
	'name': string;
	'status': number;
}

/**
 *   Объект поста
 */
interface IPost {
	'id': number;
	'date': string;
	'date_gmt': string;
	'guid': {
		'rendered': string
	};
	'modified': string;
	'modified_gmt': string;
	'slug': string;
	'status': string;
	'type': string;
	'link': string;
	'title': {
		'rendered': string
	};
	'content': {
		'rendered': string,
		'protected': boolean
	};
	'excerpt': {
		'rendered': string,
		'protected': boolean
	};
	'author': number;
	'featured_media': number;
	'comment_status': string;
	'ping_status': string;
	'sticky': boolean;
	'template': string;
	'format': string;
	'meta': Array<any>;
	'categories': Array<number>;
	'tags': Array<number>;
	'acf': IPostACFData;
	'comment_count': number;
	'_links': Object;
}

/**
 *   Интерфейс описывающий переданные seo данные
 */
interface ISeo {
	'seo-title': string;
	'seo-description': string;
	'seo-keywords': string;
	'seo-og-title': string;
	'seo-og-description': string;
	'seo-og-image': string;
	'seo-twitter-title': string;
	'seo-twitter-description': string;
	'seo-twitter-image': string;
	'seo-meta': Array<MetaDefinition>;
}

/**
 *   ACF данные, которые принадлежат посту
 */
interface IPostACFData extends ISeo {
	'miracle-post-game': {
			'value': string,
			'label': string
		};
	'miracle-post-primary-tag': IMiracleACFObjectData;
	'miracle-post-tags': Array<IMiracleACFObjectData>;
	'miracle-post-category': IMiracleACFObjectData;
	'miracle-post-image': IMiracleACFImageData;
}

/**
 *   Формат, в котором ACF возвращает данные в виде объекта
 */
interface IMiracleACFObjectData {
	'term_id': number;
	'name': string;
	'slug': string;
	'term_group': number;
	'term_taxonomy_id': number;
	'taxonomy': string;
	'description': string;
	'parent': number;
	'count': number;
	'filter': string;
}

/**
 *   Формат, в котором ACF возвращает данные о изображении в виде объекта
 */
interface IMiracleACFImageData {
	'ID': number;
	'id': number;
	'title': string;
	'filename': string;
	'url': string;
	'alt': string;
	'author': string;
	'description': string;
	'caption': string;
	'name': string;
	'date': string;
	'modified': string;
	'mime_type': string;
	'type': string;
	'icon': string;
	'width': number;
	'height': number;
	'sizes': {
		'thumbnail': string,
		'thumbnail-width': number,
		'thumbnail-height': number,
		'medium': string,
		'medium-width': number,
		'medium-height': number,
		'medium_large': string,
		'medium_large-width': number,
		'medium_large-height': number,
		'large': string,
		'large-width': number,
		'large-height': number
	};
}

interface IPosts {
	'date': Date;
	'day_posts': Array<IPost>;
}

interface IStream {
	'average_fps': number;
	'broadcast_platform': string;
	'channel': IChannel;
	'community_id': string;
	'community_ids': Array<any>;
	'created_at': string;
	'delay': number;
	'game': string;
	'is_playlist': boolean;
	'preview': {
		'large': string,
		'medium': string,
		'small': string,
		'template': string
	};
	'stream_type': string;
	'video_height': number;
	'viewers': number;
	'_id': number;
}

interface IChannel {
	'broadcaster_language': string;
	'broadcaster_type': string;
	'created_at': string;
	'description': string;
	'display_name': string;
	'followers': number;
	'game': string;
	'language': string;
	'logo': string;
	'mature': boolean;
	'name': string;
	'partner': boolean;
	'profile_banner': string;
	'profile_banner_background_color': string;
	'status': string;
	'updated_at': string;
	'url': string;
	'video_banner': string;
	'views': number;
	'_id': number;
}

interface IStreams {
	'streams': Array<IStream>;
}

interface IUser {
	'id': number;
	'name': string;
	'url': string;
	'description': string;
	'link': string;
	'slug': string;
	'avatar_urls': {
		'24': string,
		'48': string,
		'96': string
	};
	'meta': Array<any>;
	'acf': Array<any>;
	'_links': Object;
}

interface IComment {
	'id': number;
	'post': number;
	'parent': number;
	'author': number;
	'author_name': string;
	'author_url': string;
	'date': string;
	'date_gmt': string;
	'content': {
		// В формате HTML
		'rendered': string
	};
	'link': string;
	'status': string;
	'type': string;
	'author_avatar_urls': {
		'24': string,
		'48': string,
		'96': string
	};
	'meta': Array<any>;
	'acf': Array<any>;
	'_links': Object;
}

interface ICategory {
	'id': number;
	'count': number;
	'description': string;
	'link': string;
	'name': string;
	'slug': string;
	'taxonomy': string;
	'parent': number;
	'meta': Array<any>;
	'acf': IACF;
	'_links': Object;
}

interface ITag {
	'id': number;
	'count': number;
	'description': string;
	'link': string;
	'name': string;
	'slug': string;
	'taxonomy': string;
	'parent': number;
	'meta': Array<any>;
	'acf': Array<any>;
	'_links': Object;
}

interface IPage {
	'id': number;
	'date': string;
	'date_gmt': string;
	'guid': {
		'rendered': string
	};
	'modified': string;
	'modified_gmt': string;
	'slug': string;
	'status': string;
	'type': string;
	'link': string;
	'title': {
		'rendered': string
	};
	'content': {
		'rendered': string,
		'protected': boolean
	};
	'excerpt': {
		'rendered': string,
		'protected': boolean
	};
	'author': number;
	'featured_media': number;
	'parent': number;
	'menu_order': number;
	'comment_status': string;
	'ping_status': string;
	'template': string;
	'meta': Array<any>;
	'acf': IACF;
	'_links': Object;
}

interface ICalendarOption {
	'timepicker': boolean;
	'language': string;
}

interface IACF extends ISeo {
	[propName: string]: any;
}

export {
	ILogo,
	IMenuItem,
	IMenuStore,
	IMatch,
	IMatchMember,
	IPost,
	IPostACFData,
	IMiracleACFObjectData,
	IPosts,
	IChannel,
	IStream,
	IStreams,
	IUser,
	IComment,
	ICategory,
	ITag,
	IPage,
	ICalendarOption,
	ISeo
};
