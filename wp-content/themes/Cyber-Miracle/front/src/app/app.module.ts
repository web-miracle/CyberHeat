import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { ServiceModule } from './service/service.module';
import { PluginsModule } from './module/plugins.module';
import { AppRoutingModule } from './module/app-routing.module';

// Components, Directive, Pipe
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { DesktopHomeComponent } from './components/home/desktop/desktop.home.components';
import { TabletHomeComponent } from './components/home/tablet/tablet.home.components';

import { MiracleRadioDirective } from './directive/miracle-radio.directive';
import { NewsComponent } from './components/news/news.component';
import { CategoryComponent } from './components/category/category.component';
import { TagComponent } from './components/tag/tag.component';
import { PostComponent } from './components/post/post.component';
import { ForecastComponent } from './components/forecast/forecast.component';
import { ArchiveForecastComponent } from './components/archive-forecast/archive-forecast.component';
import { ArchiveTourComponent } from './components/archive-tour/archive-tour.component';
import { TourComponent } from './components/tour/tour.component';
import { ErrorComponent } from './components/error/error.component';
import { NotFoundComponent } from './components/404/404.component';
import { ScrollMenuDirective } from './directive/scroll-menu.directive';
import { CommentComponent } from './structure-components/comment/comment.component';
import { SingleCommentComponent } from './structure-components/single-comment/single-comment.component';
import { HomePostsDirective } from './directive/home-posts.directive';
import { ScrollToDirective } from './directive/scroll-to.directive';
import { StreamComponent } from './components/stream/stream.component';
import { SingleStreamComponent } from './components/single-stream/single-stream.component';
import { ProfileComponent } from './components/profile/profile.component';

import { NavigationComponent } from './structure-components/navigation/navigation.component';
import { AuthModalComponent } from './structure-components/auth-modal/auth-modal.component';
import { ModalImageComponent } from './structure-components/modal-image/modal-image.component';

@NgModule({
	'declarations': [
		AppComponent,
		HomeComponent,
		MiracleRadioDirective,
		NewsComponent,
		CategoryComponent,
		PostComponent,
		ForecastComponent,
		ArchiveForecastComponent,
		ArchiveTourComponent,
		TourComponent,
		ErrorComponent,
		ScrollMenuDirective,
		CommentComponent,
		SingleCommentComponent,
		TagComponent,
		HomePostsDirective,
		ScrollToDirective,
		StreamComponent,
		SingleStreamComponent,
		DesktopHomeComponent,
		TabletHomeComponent,
		NavigationComponent,
		AuthModalComponent,
		NotFoundComponent,
		ProfileComponent,
		ModalImageComponent
	],
	'imports': [
		BrowserModule,
		PluginsModule,
		ServiceModule,
		AppRoutingModule
	],
	'providers': [],
	'bootstrap': [AppComponent]
})
export class AppModule {}

import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import localeRuExtra from '@angular/common/locales/extra/ru';

registerLocaleData(localeRu, 'ru-RU', localeRuExtra);
