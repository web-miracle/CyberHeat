/**
 *   В данном файле конфигурации приложения
 */
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

/**
 *   Домен REST API
 *   @type {String}
 */

const domain = 'http://cyberheat.net';

/**
 *   Использовать localStorage,
 *   подробнее https://developer.mozilla.org/ru/docs/Web/API/Window/localStorage
 *   @type {Boolean}
 */
const uselocalStorage = false;

/**
 *   Конфигурации по умолчанию для SwiperSlider
 *   подробнее https://github.com/zefoy/ngx-swiper-wrapper
 *   @type {SwiperConfigInterface}
 */
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
	'direction': 'horizontal',
	'loop': false,
	'navigation': {
		'nextEl': '.last-game__button-next',
		'prevEl': '.last-game__button-prev',
		'disabledClass': 'last-game__button-disabled'
	},
	'breakpoints': {
		'400': {
			'slidesPerView': 1
		},
		'500': {
			'slidesPerView': 2
		},
		'650': {
			'slidesPerView': 3
		},
		'800': {
			'slidesPerView': 2
		},
		'1000': {
			'slidesPerView': 3
		},
		'1200': {
			'slidesPerView': 4
		},
		'1400': {
			'slidesPerView': 5
		},
		'3000': {
			'slidesPerView': 6
		}
	}
};

export { domain, uselocalStorage, DEFAULT_SWIPER_CONFIG };
