import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AirOptions } from './lib/options';
import { AirCalendar } from './lib/calendar';
import { LANGUAGES, AirLanguage } from './lib/languages';

@Component({
	'selector': 'app-air-datepicker',
	'templateUrl': './air-datepicker.component.html',
	'styleUrls': ['./style.css']
})
export class AirDatepickerComponent implements OnInit {
	@Input() airOptions: AirOptions;
	@Input() airDate: Date;
	// @Input() airDateArray: Array<Date> = [];
	@Input() startSelected = true;

	@Output() airChange = new EventEmitter<Date>();
	// @Output() airChangeArray = new EventEmitter<Array<Date>>();

	airLanguage: AirLanguage;
	airCalendar: AirCalendar;

	ngOnInit () {
		this.airOptions = new AirOptions(this.airOptions || {} as AirOptions);
		this.airLanguage = LANGUAGES.get(this.airOptions.language);
		this.airCalendar = new AirCalendar(this.airDate, this.airOptions, this.startSelected);
	}

	setDate (index?: number) {
		if (this.airCalendar.airDays[index] && !this.airCalendar.airDays[index].disabled) {
			this.airCalendar.selectDate(index);
			this.setTime();
		}
	}

	setTime () {
		this.airDate
			.setTime(+ Date.UTC(
						this.airCalendar.year,
						this.airCalendar.month,
						this.airCalendar.date,
						this.airCalendar.hour,
						this.airCalendar.minute
						)
					);

		this.airChange.emit(this.airDate);

		/*
		if( this.airDateArray.length == 2 || this.airDateArray.length == 0 ){
			this.airDateArray = [new Date()];
			this.airDateArray[0]
				.setTime(+ Date.UTC(
								this.airCalendar.year,
								this.airCalendar.month,
								this.airCalendar.date,
								this.airCalendar.hour,
								this.airCalendar.minute
							)
						);
		}else{
			this.airDateArray[1] = this.airDate;
			this.airChangeArray.emit(this.airDateArray);
		}
		*/
	}
}
