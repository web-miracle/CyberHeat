// Normalizes month/year
class AirMonth {
	month: number;
	year: number;

	constructor (month, year) {
		if (month > 11) {
			year++;
			month = 0;
		} else if (month < 0) {
			year--;
			month = 11;
		}

		this.month = month;
		this.year = year;
	}
}

class AirDay {
	date: number;
	weekend: boolean;
	other: boolean;
	current: boolean;
	selected: boolean;
	disabled: boolean;

	constructor (date: number, weekend = false, disabled = false, other = false, current = false, selected = false) {
		this.date = date;
		this.weekend = weekend;
		this.disabled = disabled;
		this.other = other;
		this.current = current;
		this.selected = selected;
	}
}

class AirWeekend {
	day: number;

	constructor (day: number = 0) {
		this.day = day;
	}

	progress (): boolean {
		let weekend = false;

		if (this.day === 5 /* Saturday */) {
			weekend = true;
			++this.day;
		} else if (this.day === 6 /* Sunday */) {
			weekend = true;
			this.day = 0; // It's a new week!
		} else {
			++this.day;
		}

		return weekend;
	}
}

export { AirWeekend, AirMonth, AirDay };
