import { AirOptions } from './options';
import { AirWeekend, AirMonth, AirDay } from './assetsClass';

export class AirCalendar {
	daysInMonth: Array<number> = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
	airOptions: AirOptions;
	airDays: Array<AirDay>;
	year: number;
	month: number;
	date: number;
	hour: number;
	minute: number;

	constructor (date: Date = new Date, airOptions: AirOptions = new AirOptions, startSelected: boolean = true) {
		this.airOptions = airOptions;
		this.year = date.getFullYear();
		this.month = date.getMonth();
		this.date = date.getDate();
		this.hour = date.getHours();
		this.minute = date.getMinutes();
		this.updateCalendar(startSelected);
	}

	next () {
		this.setMonth(this.month + 1);
		this.updateCalendar();
	}

	previous () {
		this.setMonth(this.month - 1);
		this.updateCalendar();
	}

	updateCalendar (selectDate = false) {
		this.airDays = [];
		const daysInMonth = this.getDaysInMonth(this.month);
		const date = new Date;
		const firstDayOfMonth: number = ((new Date(Date.parse(`${this.year}/${this.month + 1}/1`))).getDay() || 7) - 1; // Making 0 === monday
		const weekend: AirWeekend = new AirWeekend(firstDayOfMonth);

		if (firstDayOfMonth/* is not monday (0) */) {
			const daysInLastMonth = this.getDaysInMonth(this.month - 1);
			const initAirMonth = new AirMonth(this.month - 1, this.year);
			for (
				let dayCount = daysInLastMonth - firstDayOfMonth;
				dayCount < daysInLastMonth;
				dayCount++
			) {
				this.airDays
					.push(
						new AirDay(
							dayCount,
							weekend.progress(),
							this.airOptions
								.isDisabled(new Date(`${initAirMonth.year}/${initAirMonth.month + 1}/${dayCount}`)),
							true
						)
					);
			}
		}

		for (let dayIteration = 1; dayIteration <= daysInMonth; dayIteration++) {
			this.airDays
				.push(
					new AirDay(
						dayIteration,
						weekend.progress(),
						this.airOptions
							.isDisabled(
								new Date(`${this.year}/${this.month + 1}/${dayIteration}`)
							)
					)
				);
		}

		if (this.date > daysInMonth) {
			this.date = daysInMonth; // Select the maximum available this month instead
		}

		// Select the calendar date; usually at calendar initialisation; only if enabled
		if (selectDate) {
		  const selectedDate = firstDayOfMonth + this.date - 1;
		  this.airDays[selectedDate].selected = !this.airDays[selectedDate].disabled;
		}

		if (date.getMonth() === this.month) {
			// Set the current date if it's the current month, regardless of year for now
			this.airDays[firstDayOfMonth + date.getDate() - 1].current = true;
		}

		const daysSoFar = firstDayOfMonth + daysInMonth;
		const airMonth = new AirMonth(this.month + 1, this.year);
		for (let dayIteration = 1; dayIteration <= (daysSoFar > 35 ? 42 : 35) - daysSoFar; dayIteration++) {
			this.airDays
				.push(
					new AirDay(
						dayIteration,
						weekend.progress(),
						this.airOptions
							.isDisabled(
								new Date(`${airMonth.year}/${airMonth.month + 1}/${dayIteration}`)
							),
						true
					)
				);
		}
	}

	selectDate (index: number) {
		this.date = this.airDays[index].date;

		// Might be a day from the previous/next month
		if (index < 7 && this.date > 20) {
			this.previous();
		} else if (index > 20 && this.date < 8) {
			this.next();
		} else {
			// No need to update the whole calendar here
			for (const day of this.airDays) {
				if (day.selected) {
					day.selected = false;
				}
			}

			this.airDays[index].selected = true;
		}
	}

	setMonth (month) {
		const airMonth: AirMonth = new AirMonth(month, this.year);
		this.month = airMonth.month;
		this.year = airMonth.year;
	}

	getDaysInMonth (month) {
		const airMonth: AirMonth = new AirMonth(month, this.year);
		if (airMonth.month === 1 && ((airMonth.year % 4 === 0) && (airMonth.year % 100 !== 0)) || (airMonth.year % 400 === 0)) {
			return 29;
		}

		return this.daysInMonth[airMonth.month];
	}
}
