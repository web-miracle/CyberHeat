import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './../components/home/home.component';
import { NewsComponent } from './../components/news/news.component';
import { CategoryComponent } from './../components/category/category.component';
import { TagComponent } from './../components/tag/tag.component';
import { PostComponent } from './../components/post/post.component';
import { ForecastComponent } from './../components/forecast/forecast.component';
import { ArchiveForecastComponent } from './../components/archive-forecast/archive-forecast.component';
import { TourComponent } from './../components/tour/tour.component';
import { ArchiveTourComponent } from './../components/archive-tour/archive-tour.component';
import { StreamComponent } from './../components/stream/stream.component';
import { SingleStreamComponent } from './../components/single-stream/single-stream.component';
import { ProfileComponent } from './../components/profile/profile.component';
import { ErrorComponent } from './../components/error/error.component';
import { NotFoundComponent } from './../components/404/404.component';

/**
 *   Главный объект роутинга, без шапки страници
 *   @type {Routes}
 */
const routes: Routes =
	[
		/**
		 *   Одна новость, где
		 *   categorySlug - слуг категори, в которой находится страница
		 *   postSlug     - слуг поста
		 */
		{
			'path': 'news/:categorySlug/:postSlug',
			'component': PostComponent
		},

		/**
		 *   Страница категории новостей, где
		 *   categorySlug - слуг категории
		 */
		{
			'path': 'news/:categorySlug',
			'component': CategoryComponent
		},

		/**
		 *   Страница всех постов
		 */
		{
			'path': 'news',
			'component': NewsComponent
		},

		/**
		 *   Страница новостей тега, где
		 *   cagSlug - слуг категории
		 */
		{
			'path': 'tag/:tagSlug',
			'component': TagComponent
		},

		/**
		 *   Страница стрима
		 */
		{
			'path': 'stream/:channel',
			'component': SingleStreamComponent
		},

		/**
		 *   Страница всех стримов
		 */
		{
			'path': 'stream',
			'component': StreamComponent
		},

		/**
		 *   Страница статистики матчей
		 */
		{
			'path': 'statistic/match',
			'component': ErrorComponent
		},

		/**
		 *   Страница статистики рейтингов
		 */
		{
			'path': 'statistic/broker',
			'component': ErrorComponent
		},

		/**
		 *   Страница статистики прогнозов
		 */
		{
			'path': 'statistic/forecast',
			'component': ErrorComponent
		},

		/**
		 *   Страница статистики турниров
		 */
		{
			'path': 'statistic/tour',
			'component': ErrorComponent
		},

		/**
		 *   Страница статистики
		 */
		{
			'path': 'statistic',
			'component': ErrorComponent
		},

		/**
		 *   Страница аналитический центр
		 */
		{
			'path': 'analitic',
			'component': ErrorComponent
		},

		/**
		 *   Страница форума
		 */
		{
			'path': 'forum',
			'component': ErrorComponent
		},

		/**
		 *   Страница турнира, где
		 *   tourSlug - слуг турнира
		 */
		{
			'path': 'tour/:tourSlug',
			'component': ErrorComponent
		},

		/**
		 *   Страница всех турниров
		 */
		{
			'path': 'tour',
			'component': ErrorComponent
		},

		/**
		 *   Прогноз по матчу, где
		 *   gameSlug - слуг матча
		 */
		{
			'path': 'forecast/game/:gameSlug',
			'component': ErrorComponent
		},

		/**
		 *   Cтраница профиля пользователя
		 */
		{
			'path': 'user/profile',
			'component': ProfileComponent
		},

		/**
		 *   Cтраница о пользователя
		 */
		{
			'path': 'user/:userSlug',
			'component': ErrorComponent
		},

		/**
		 *   Главная страница
		 */
		{
			'path': '',
			'component': HomeComponent
		},

		/**
		 *   Все страници не попавшие в роутинг перейдут на этот компонент
		 */
		{
			'path': '**',
			'component': NotFoundComponent
		},
	];

@NgModule({
	'imports': [RouterModule.forRoot(routes)],
	'exports': [RouterModule]
})
export class AppRoutingModule {}
