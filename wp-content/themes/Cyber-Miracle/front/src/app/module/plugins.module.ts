/**
 *   В данном модуле подключены все постороние разширения и плагины
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { SwiperModule, SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { domain, DEFAULT_SWIPER_CONFIG } from './../options';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';


/*import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../../environments/environment';*/

import { WordPressModule } from 'ngx-wordpress';
import { FormsModule } from '@angular/forms';
import { AirDatepickerComponent } from './angular2-air-datepicker/index';

@NgModule({
	'imports': [
		CommonModule,
		HttpModule,
		SwiperModule,
		HttpClientModule,
		InlineSVGModule,
		FormsModule,
		BrowserAnimationsModule,
		WordPressModule.forRoot(domain),
		/*ServiceWorkerModule.register('/ngsw-worker.js', {'enabled': environment.production})
		environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : []*/
	],
	'providers': [
		{
			'provide': SWIPER_CONFIG,
			'useValue': DEFAULT_SWIPER_CONFIG
		}
	],
	'declarations': [
		AirDatepickerComponent
	],
	'exports': [
		HttpModule,
		SwiperModule,
		InlineSVGModule,
		WordPressModule,
		BrowserAnimationsModule,
		FormsModule,
		AirDatepickerComponent
	]
})
export class PluginsModule {}
