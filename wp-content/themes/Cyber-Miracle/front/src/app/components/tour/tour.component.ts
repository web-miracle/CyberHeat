import { Component, HostBinding, OnInit } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { ActivatedRoute } from '@angular/router';

@Component({
	'selector': 'app-tour',
	'templateUrl': './tour.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class TourComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {}
}
