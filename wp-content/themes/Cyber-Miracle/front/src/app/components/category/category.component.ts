import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { slideInLeftAnimation, newsAnimation } from '../../animation/animate';
import { CategoryService } from '../../service/category.service';
import { PostService } from '../../service/posts.service';
import { IPost, IPosts, ICategory, ICalendarOption } from '../../interface/interface';
import { SVGCacheService } from 'ng-inline-svg';
import { LoggedInUserService, LoadPage } from '../../service/global-vars.service';
import { domain } from '../../options';
import { SeoService } from '../../service/seo.service';

@Component({
	'selector': 'app-category',
	'templateUrl': './category.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation, newsAnimation]
})
export class CategoryComponent implements OnInit, OnDestroy {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	posts: Array<IPost> = [];
	category: ICategory = null;

	options: ICalendarOption = {
			'timepicker': false,
			'language': 'ru'
		};

	date: Date = new Date();

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		public svgService: SVGCacheService,
		private catService: CategoryService,
		private postService: PostService,
		public loadPage: LoadPage,
		private seoService: SeoService
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });
	}

	ngOnInit() {
		this.route
			.params
			.mergeMap((res: Params) => {
				this.loadPage.set('true');

				return this.catService.getCategoryByslug(res['categorySlug']);
			})
			.map((cat: Array<ICategory>) => {
				if (cat.length < 0) {
					this.router.navigate(['/404'], { 'replaceUrl': true });

					return null;
				} else {
					return cat[0];
				}
			})
			.map((cat: ICategory) => {
				const url: string = domain + this.router.url + '/';
				this.seoService.setSEOTags(cat.acf, url);

				return cat;
			})
			.subscribe(
				(cat: ICategory) => {
					this.initPosts(cat);
				},
				(error) => {
					this.router.navigate(['/404'], { 'replaceUrl': true });
				}
			);
	}

	initPosts(cat: ICategory): void {
		const today: Date = new Date();
		let after: Date = new Date();
		after = new Date(after.setDate(today.getDate() - 5));

		const query: string = '?categories=' + cat.id + '&per_page=100&after=' + after.toISOString();
		this.postService
			.getPostQuery(query)
			.subscribe(
				(res: Array<IPost>) => {
					/*if (res.length > 0) {*/
						this.category = cat;
						this.posts = res;
						this.loadPage.set('false');
					/*} else {
						this.router.navigate(['/404'], { 'replaceUrl': true });
					}*/
				},
				(error) => {
					this.router.navigate(['/404'], { 'replaceUrl': true });
				}
			);

	}

	ngOnDestroy() {
		this.loadPage.set('true');
	}

	dateChanged(date): void {
		this.loadPage.set('true');
		const query: string = '?categories=' + this.category.id + '&per_page=100&' + this.dateParams(this.date);
		this.postService
			.getPostQuery(query)
			.subscribe((res: Array<IPost>) => {
				this.posts = res;
				this.loadPage.set('false');
			});
	}

	dateParams(date: Date): string {
		const year: string = date.getFullYear().toString();
		const month: string = (date.getMonth() < 10) ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
		const day: string = (date.getDate() < 10) ? '0' + date.getDate().toString() : date.getDate().toString();

		return 'after=' + year + '-' + month + '-' + day + 'T00:00:00&before=' + year + '-' + month + '-' + day + 'T23:55:59';
	}

	sortPosts(a: IPosts, b: IPosts): number {
		if ((a.date.getTime() - b.date.getTime()) > 0) {
			return -1;
		} else if ((a.date.getTime() - b.date.getTime()) < 0) {
			return 1;
		}

		return 0;
	}
}
