import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';

@Component({
	'selector': 'app-error',
	'templateUrl': './error.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class ErrorComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor() {}

	ngOnInit() {}
}
