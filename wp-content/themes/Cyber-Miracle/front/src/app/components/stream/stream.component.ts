import { Component, HostBinding, OnInit } from '@angular/core';
import { slideInLeftAnimation, streamAnimation } from '../../animation/animate';
import { StreamService } from '../../service/stream.service';
import { LoadPage } from '../../service/global-vars.service';
import { IStreams, IStream } from '../../interface/interface';

@Component({
	'selector': 'app-stream',
	'templateUrl': './stream.component.html',
	'styleUrls': ['./stream.component.scss'],
	'animations': [ slideInLeftAnimation, streamAnimation ]
})
export class StreamComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	categories: Array<any> = [];
	activeCategory = 'all';
	streams: Array<any>;

	constructor(
		public loadPage: LoadPage,
		private streamService: StreamService
	) {
		this.categories =
		[
			{
				'name': 'Все',
				'slug': 'all'
			},
			{
				'name': 'Dota 2',
				'slug': 'dota 2'
			},
			{
				'name': 'CS:GO',
				'slug': 'counter-strike: global offensive'
			},
			{
				'name': 'Other',
				'slug': 'other'
			}
		];
	}

	ngOnInit() {
		this.streamService
			.getStream()
			.subscribe((res: IStreams) => {
				res.streams
					.forEach((stream: IStream) => {
						stream['show'] = this.showStream(stream.game);
					});

				this.streams = res.streams;
				this.loadPage.set('false');
			});
	}

	activeCat(cat: string): void {
		this.activeCategory = cat;
		this.streams
			.forEach((stream: IStream) => {
				stream['show'] = this.showStream(stream.game);
			});
	}

	showStream(game: string = ''): boolean {
		if (this.activeCategory === 'all') {
			return true;
		} else if (this.activeCategory.toLowerCase() === game.toLowerCase()) {
			return true;
		} else if (
			this.activeCategory === 'other' &&
			(game.toLowerCase() !== 'counter-strike: global offensive' &&
			game.toLowerCase() !== 'dota 2')) {
			return true;
		}

		return false;
	}
}
