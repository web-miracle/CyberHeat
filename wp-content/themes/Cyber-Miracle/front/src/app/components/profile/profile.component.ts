import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { LoadPage } from '../../service/global-vars.service';

@Component({
	'selector': 'app-profile',
	'templateUrl': './profile.component.html',
	'styleUrls': ['./profile.component.scss'],
	'animations': [ slideInLeftAnimation ]
})
export class ProfileComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor(
		public load: LoadPage
	) {}

	ngOnInit() {
		this.load.set('false');
	}
}
