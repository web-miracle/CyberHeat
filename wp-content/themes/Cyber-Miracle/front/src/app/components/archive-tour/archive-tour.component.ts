import { Component, HostBinding, OnInit } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { ActivatedRoute } from '@angular/router';

@Component({
	'selector': 'app-archive-tour',
	'templateUrl': './archive-tour.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class ArchiveTourComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {}
}
