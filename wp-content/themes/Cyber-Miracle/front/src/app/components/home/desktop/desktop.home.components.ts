import { Component, HostBinding, OnInit, Renderer2 } from '@angular/core';
import { PostService } from '../../../service/posts.service';
import { StreamService } from '../../../service/stream.service';
import { CategoryService } from '../../../service/category.service';
import { IPost, IPosts, IStream, IStreams, ICategory } from '../../../interface/interface';
import { domain } from '../../../options';
import { LoggedInUserService, LoadPage } from '../../../service/global-vars.service';
import { HomePostsDirective } from '../../../directive/home-posts.directive';

import { SVGCacheService } from 'ng-inline-svg';

@Component({
	'selector': 'app-desktop-home',
	'templateUrl': './desktop.home.components.html',
	'styles': [ ':host { position: relative; }' ]
})
export class DesktopHomeComponent implements OnInit {

	/**
	 *   Часовой пояс
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-16
	 */
	timeZone: string = (new Date()).toString().split('GMT')[1].split(' (')[0];

	/**
	 *   Массив постов по деням
	 *   @type {Array<IPosts>}
	 */
	posts: Array<IPosts> =
		[{
			'date': new Date(),
			'day_posts': []
		}];
	next_posts: Array<IPosts> = [];

	/**
	 *   Массив постов за сегодняшний день
	 *   @type {IPosts}
	 */
	todayPosts: Array<IPosts> =
		[{
			'date': new Date(),
			'day_posts': []
		}];

	/**
	 *   Массив постов из категории `видео`
	 *   @type {Array<Object>}
	 */
	videoPosts: Array<Object> = [];
	next_videoPosts: Array<Object> = [];

	/**
	 *   Массив последних шести горячих новостей
	 *   @type {Array<Object>}
	 */
	primaryPosts: Array<Object>;

	/**
	 *   Массив стримов
	 *   @type {Array<IStream>}
	 */
	stream: Array<IStream>;

	/**
	 *   Количество постов на странице
	 *   @type {Number}
	 */
	per_page = 50;

	/**
	 *   Количество видео постов на странице
	 *   @type {Number}
	 */
	video_per_page = 10;

	/**
	 *   Страница вывода постов, первая страница загружается сразу
	 *   @type {number}
	 */
	viewPosts = 2;

	/**
	 *   Страница вывода видео постов, первая страница загружается сразу
	 *   @type {number}
	 */
	viewVideoPosts = 2;

	/**
	 *   Свойство отображающие, подгрузку на данный момент новые посты при доскролле
	 *   @type {boolean}
	 */
	scrollAdd = true;

	/**
	 *   Определяет размер экрана
	 */
	windowWidth: number;

	/**
	 *   Информация о категории видео
	 */
	videoCat: ICategory;

	/**
	 *   При создании компонента задается путь к svg картинкам для плагина `ng-inline-svg`
	 *   	подробнее: https://www.npmjs.com/package/ng-inline-svg
	 *   	из замечаний проблемы с кросбраузерными запросами
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @param   {PostService}     private postService   Сервис для получения постов приложения
	 *   @param   {StreamService}   private streamService Сервис для получения стримов
	 *   @param   {SVGCacheService} public  svgService    Плагин `ng-inline-svg`
	 *   @param   {LoadPage}        public  LoadPage      Сервис отображающий загрузку роутинга
	 */
	constructor(
		private postService: PostService,
		private streamService: StreamService,
		public svgService: SVGCacheService,
		private catService: CategoryService,
		public loadPage: LoadPage,
		private render2: Renderer2
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });

		this.windowWidth = window.screen.width;

		render2.listen('window', 'resize', (e) => {
				this.windowWidth = window.screen.width;
			});
	}

	/**
	 *   При инициализации компонента подгружаются премиумные посты,
	 *   последние 30 постов и стримы
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	ngOnInit() {
		this.postService
			.getPrimaryPosts()
			.subscribe((res: Array<Object>) => {
				this.primaryPosts = res;
			});

		this.initPosts();

		this.streamService
			.getStream()
			.subscribe((res: IStreams) => {
				this.stream = res.streams;
				// Стримы грузятся дольше всех. Повесил выключение лоадера тут
				this.loadPage.set('false');
			});
	}

	/**
	 *   Подгружаемые посты при инициализации компонента
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	initPosts(): void {
		this.postService
			.getPostQuery('?per_page=' + this.per_page)
			.map((posts: Array<IPost>) => {
				let date: Date = new Date();

				let i = 0;
				let j = 0;
				let first = true;
				let firstT = true;
				let todayPostsCount = 6;
				if (this.windowWidth < 1400 && this.windowWidth > 1100) {
					todayPostsCount = 5;
				} else if (this.windowWidth < 1100) {
					todayPostsCount = 4;
				}

				/**
				 *   Итератор добавления в массив todayPosts
				 *   @type {Number}
				 */
				let x = 0;

				posts.forEach(
					(post, index) => {
						if (post.acf['miracle-post-category'].name !== 'Видео') {
							if (x < todayPostsCount) {

								if (firstT && ((new Date(post.date)).getDate() !== date.getDate())) {
									this.todayPosts[0].date = new Date(post.date);
								}

								if (firstT) {
									firstT = false;
								}

								if ((new Date(post.date)).getDate() === date.getDate()) {
									this.todayPosts[i].day_posts.push(post);
								} else {
									i++;

									date = new Date(post.date);
									this.todayPosts[i] = {
														'date': date,
														'day_posts': [post]
													};
								}

							} else {
								if ((new Date(post.date)).getDate() === date.getDate()) {
									if (first) {
										this.posts[0].date = new Date(post.date);
										first = false;
									}
									this.posts[j].day_posts.push(post);
								} else {
									j++;
									date = new Date(post.date);
									this.posts[j] = {
														'date': date,
														'day_posts': [post]
													};
								}
							}
							x++;
						}
					});

				return posts;
			})
			.subscribe((res) => {
				this.addNextPosts();
			});

		this.catService
			.getCategoryByslug('video')
			.map((cat: Array<ICategory>) => {
				if (cat.length < 0) {
					return null;
				} else {
					return cat[0];
				}
			})
			.map((cat: ICategory) => {
				this.videoCat = cat;
				return cat;
			})
			.mergeMap((cat: ICategory) => {
				const query: string = '?per_page=' + this.video_per_page + '&categories=' + cat.id;
				return this.postService.getPostQuery(query);
			})
			.subscribe((posts: Array<IPost>) => {
				posts.forEach(
						(post, index) => {
							if (post.acf['miracle-post-category'].name === 'Видео') {
								this.videoPosts.push(post);
							}
						}
					);
				this.addNextVideoPosts();
			});
	}

	addNextPosts(): void {
		this.scrollAdd = true;
		const query: string = '?per_page=' + this.per_page + '&page=' + this.viewPosts;
		this.postService
			.getPostQuery(query)
			.subscribe(
				(posts: Array<IPost>) => {
					this.next_posts = [];
					let date = new Date();
					let j = -1;
					posts.forEach(
						(post, index) => {
							if (post.acf['miracle-post-category'].name !== 'Видео') {
								if ((new Date(post.date)).getDate() === date.getDate()) {
									this.next_posts[j].day_posts.push(post);
								} else {
									j++;

									date = new Date(post.date);
									this.next_posts[j] = {
														'date': date,
														'day_posts': [post]
													};
								}
							}
						}
					);

					this.viewPosts++;
					this.scrollAdd = false;
				},
				(error) => {
					this.scrollAdd = true;
				}
			);
	}

	addNextVideoPosts(): void {
		this.scrollAdd = true;
		this.next_videoPosts = [];
		const videoQuery: string = '?per_page=' + this.video_per_page + '&page=' + this.viewVideoPosts + '&categories=' + this.videoCat.id;
		this.postService
			.getPostQuery(videoQuery)
			.subscribe(
				(posts: Array<IPost>) => {
					posts.forEach(
							(post, index) => {
								if (post.acf['miracle-post-category'].name === 'Видео') {
									this.next_videoPosts.push(post);
								}
							}
						);
					this.viewVideoPosts++;
					this.scrollAdd = false;
				},
				(error) => {
					this.scrollAdd = false;
				}
			);
	}

	/**
	 *   Обработчик для добавления постов при доскроле до конца
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	addPostsOnScroll(): void {
		/**
		 *   Проверяем отправлен запрос на сервис для добавления новых постов при доскролле
		 *   @autor   Mishalov_Pavel
		 *   @version 1.0
		 *   @date    2018-02-08
		 */
		if (!this.scrollAdd) {

			// Выставляем, что отправлен запрос для подгрузки новых постов при доскролле

			// Перебираем посты за три дня
			this.next_posts
				.forEach(
					(item: IPosts) => {
						const lastPosts = this.posts[this.posts.length - 1];
						if (lastPosts.date.getDate() === (new Date(item.date)).getDate()) {
							item.day_posts
								.forEach((post) => {
									this.posts[this.posts.length - 1].day_posts.push(post);
								});
						} else {
							this.posts.push(item);
						}
				});

			// Перебираем посты за три дня
			this.next_videoPosts
				.forEach((item) => {
					this.videoPosts.push(item);
				});

			this.addNextPosts();
		}
	}

	/**
	 *   Обработчик для добавления видео постов при доскроле до конца
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	addVideoPostsOnScroll(): void {
		/**
		 *   Проверяем отправлен запрос на сервис для добавления новых постов при доскролле
		 *   @autor   Mishalov_Pavel
		 *   @version 1.0
		 *   @date    2018-02-08
		 */
		if (!this.scrollAdd) {
			// Выставляем, что отправлен запрос для подгрузки новых постов при доскролле

			// Перебираем video посты
			this.next_videoPosts
				.forEach((item) => {
					this.videoPosts.push(item);
				});

			this.addNextVideoPosts();
		}
	}

	/**
	 *   Функция для сортирование постов по дате их создания
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	sortPosts(a: IPosts, b: IPosts): number {
		if ((a.date.getTime() - b.date.getTime()) > 0) {
			return -1;
		} else if ((a.date.getTime() - b.date.getTime()) < 0) {
			return 1;
		}

		return 0;
	}
}
