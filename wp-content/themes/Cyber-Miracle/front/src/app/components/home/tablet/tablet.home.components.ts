import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from '../../../service/posts.service';
import { CategoryService } from '../../../service/category.service';
import { StreamService } from '../../../service/stream.service';
import { IPost, IPosts, IStream, IStreams, IPage, ICategory } from '../../../interface/interface';
import { domain } from '../../../options';
import { LoadPage } from '../../../service/global-vars.service';
import 'rxjs/add/operator/mergeMap';
import { SVGCacheService } from 'ng-inline-svg';

@Component({
	'selector': 'app-tablet-home',
	'templateUrl': './tablet.home.components.html',
	'styles': [ ':host { position: relative; }' ]
})
export class TabletHomeComponent implements OnInit, OnDestroy {

	/**
	 *   Массив постов
	 *   @type {Array<IPosts>}
	 */
	posts: Array<IPost> = [];

	/**
	 *   Массив постов из категории `видео`
	 *   @type {Array<Object>}
	 */
	videoPosts: Array<Object> = [];

	/**
	 *   Массив последних шести горячих новостей
	 *   @type {Array<Object>}
	 */
	primaryPosts: Array<Object>;

	/**
	 *   Массив стримов
	 *   @type {Array<IStream>}
	 */
	stream: Array<IStream>;
	viewStream: IStream;

	/**
	 *   При создании компонента задается путь к svg картинкам для плагина `ng-inline-svg`
	 *   	подробнее: https://www.npmjs.com/package/ng-inline-svg
	 *   	из замечаний проблемы с кросбраузерными запросами
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @param   {PostService}     private postService   Сервис для получения постов приложения
	 *   @param   {PostService}     private pageService   Сервис для получения страници
	 *   @param   {StreamService}   private streamService Сервис для получения стримов
	 *   @param   {SVGCacheService} public  svgService    Плагин `ng-inline-svg`
	 *   @param   {LoadPage}        public  LoadPage      Сервис отображающий загрузку роутинга
	 */
	constructor(
		private postService: PostService,
		private catService: CategoryService,
		private streamService: StreamService,
		public svgService: SVGCacheService,
		public loadPage: LoadPage
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });
	}

	/**
	 *   При инициализации компонента подгружаются премиумные посты,
	 *   посты за последние 5дней и стримы
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	ngOnInit() {
		this.postService
				.getPostQuery('?per_page=3&page=1')
				.subscribe((posts: Array<IPost>) => {
					this.posts = posts;
				});

		this.streamService
			.getStream()
			.subscribe((res: IStreams) => {
				this.stream = res.streams;
				this.viewStream = this.stream[0];

				// Cтримы грузятся дольше всех. Повесил выключение лоадера тут
				this.loadPage.set('false');
			});

		this.postService
			.getPrimaryPosts()
			.subscribe((res: Array<Object>) => {
				this.primaryPosts = res;
			});

		this.catService
			.getCategoryByslug('video')
			.mergeMap((res: Array<ICategory>) => {
				const query: string = '?categories=' + res[0].id + '&per_page=3&page=1';
				return this.postService.getPostQuery(query);
			})
			.subscribe((posts: Array<IPost>) => {
				this.videoPosts = posts;
			});
	}

	ngOnDestroy() {
		this.loadPage.set('true');
	}

	setViewStream(stream: IStream): void {
		this.viewStream = stream;
	}

	stripTags(str): string {
		return str.replace(/<\/?[^>]+>/gi, '');
	}
}
