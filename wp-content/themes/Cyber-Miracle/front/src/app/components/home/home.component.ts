import { Component, HostBinding, Renderer2, OnInit, OnDestroy } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { PageService } from '../../service/page.service';
import { IPage } from '../../interface/interface';
import { domain } from '../../options';
import { LoadPage } from '../../service/global-vars.service';
import { SeoService } from '../../service/seo.service';

@Component({
	'selector': 'app-home',
	'templateUrl': './home.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class HomeComponent implements OnInit, OnDestroy {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	/**
	 *   Slug главной страници
	 */

	pageSlug = 'main';

	/**
	 *   Определяет экран
	 */

	windowWidth: number;

	/**
	 *   При создании компонента задается путь к svg картинкам для плагина `ng-inline-svg`
	 *   	подробнее: https://www.npmjs.com/package/ng-inline-svg
	 *   	из замечаний проблемы с кросбраузерными запросами
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @param   {PostService}     private postService   Сервис для получения постов приложения
	 *   @param   {PostService}     private pageService   Сервис для получения страници
	 *   @param   {SVGCacheService} public  svgService    Плагин `ng-inline-svg`
	 *   @param   {LoadPage}        public  LoadPage      Сервис отображающий загрузку роутинга
	 */
	constructor(
		private pageService: PageService,
		private loadPage: LoadPage,
		private render2: Renderer2,
		private seoService: SeoService
	) {
		this.windowWidth = window.screen.width;

		render2.listen(
					'window',
					'resize',
					(e) => {
						this.windowWidth = window.screen.width;
					}
			);
	}

	/**
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 */
	ngOnInit() {
		this.pageService
			.getPage(this.pageSlug)
			.subscribe(
				(page: IPage) => {
					this.seoService.setSEOTags(page.acf);
				}
			);
	}

	ngOnDestroy() {
		this.loadPage.set('true');
	}
}
