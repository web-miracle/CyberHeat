import { Component, OnInit, HostBinding } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { slideInLeftAnimation } from '../../animation/animate';
import { domain } from '../../options';
import { Router } from '@angular/router';
import { SeoService } from '../../service/seo.service';
import { ISeo } from '../../interface/interface';

@Component({
	'selector': 'app-404',
	'templateUrl': './404.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class NotFoundComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor(
		private title: Title,
		private meta: Meta,
		private router: Router,
		private seoService: SeoService
	) {}

	ngOnInit() {
		const url: string = domain + this.router.url + '/';
		const seo: ISeo = {
								'seo-title': 'Страница не найдена',
								'seo-description': 'Данной странице не найдено, убедитесь что перешли по нужному адресу',
								'seo-keywords': '',
								'seo-og-title': 'Страница не найдена',
								'seo-og-description': 'Данной странице не найдено, убедитесь что перешли по нужному адресу',
								'seo-og-image': '',
								'seo-twitter-title': '',
								'seo-twitter-description': '',
								'seo-twitter-image': '',
								'seo-meta': []
							};
		this.seoService.setSEOTags(seo, url, '400');
	}
}
