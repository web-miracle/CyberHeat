import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { slideInLeftAnimation, newsAnimation } from '../../animation/animate';
import { PostService } from '../../service/posts.service';
import { PageService } from '../../service/page.service';
import { IPost, IPosts, IPage } from '../../interface/interface';
import { SVGCacheService } from 'ng-inline-svg';
import { LoggedInUserService, LoadPage } from '../../service/global-vars.service';
import { domain } from '../../options';
import { SeoService } from '../../service/seo.service';

@Component({
	'selector': 'app-news',
	'templateUrl': './news.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [ slideInLeftAnimation, newsAnimation ]
})
export class NewsComponent implements OnInit, OnDestroy {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	posts: Array<IPost> = [];

	pageSlug = 'news';

	options: Object =
		{
			'timepicker': false,
			'language': 'ru'
		};

	date: Date = new Date;

	constructor(
		private router: Router,
		private pageService: PageService,
		public svgService: SVGCacheService,
		private postService: PostService,
		public loadPage: LoadPage,
		private seoService: SeoService
	) {
		const url: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': url });
	}

	ngOnInit() {
		this.initPosts();

		this.pageService
			.getPage(this.pageSlug)
			.subscribe((page: IPage) => {
				const url: string = domain + this.router.url + '/';
				this.seoService.setSEOTags(page.acf, url);
			});
	}

	ngOnDestroy(): void {
		this.loadPage.set('true');
	}

	initPosts(): void {
		const day: Date = new Date();
		const query: string = '?per_page=100&' + this.dateParams(day);

		this.postService
			.getPostQuery(query)
			.subscribe((res) => {
				this.posts = res;
				this.loadPage.set('false');
			});
	}

	dateChanged($event: Event): void {
		this.loadPage.set('true');
		const query: string = '?per_page=100&' + this.dateParams(this.date);
		this.postService
			.getPostQuery(query)
			.subscribe((res) => {
				this.posts = res;
				this.loadPage.set('false');
			});
	}

	dateParams(date: Date): string {
		const year: string = date.getFullYear().toString();
		const month: string = (date.getMonth() < 10) ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
		const day: string = (date.getDate() < 10) ? '0' + date.getDate().toString() : date.getDate().toString();

		return 'after=' + year + '-' + month + '-' + day + 'T00:00:00&before=' + year + '-' + month + '-' + day + 'T23:55:59';
	}

}
