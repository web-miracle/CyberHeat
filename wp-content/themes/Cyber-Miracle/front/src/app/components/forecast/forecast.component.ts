import { Component, HostBinding, OnInit } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
	'selector': 'app-forecast',
	'templateUrl': './forecast.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation]
})
export class ForecastComponent implements OnInit {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	game: string;

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {
		this.route
			.params
			.subscribe((res: Params) => {
				this.game = res['gameSlug'];
			});
	}
}
