import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { slideInLeftAnimation, newsAnimation } from '../../animation/animate';
import { TagService } from '../../service/tag.service';
import { PostService } from '../../service/posts.service';
import { AcfService } from '../../service/acf.service';
import { IPost, IPosts, ITag, ICalendarOption } from '../../interface/interface';
import { SVGCacheService } from 'ng-inline-svg';
import { LoggedInUserService, LoadPage } from '../../service/global-vars.service';
import { domain } from '../../options';
import { SeoService } from '../../service/seo.service';

@Component({
	'selector': 'app-tag',
	'templateUrl': './tag.component.html',
	'styles': [ ':host { position: relative; }' ],
	'animations': [slideInLeftAnimation, newsAnimation]
})
export class TagComponent implements OnInit, OnDestroy {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	posts: Array<IPost> = [];
	tag: ITag;

	options: ICalendarOption =
		{
			'timepicker': false,
			'language': 'ru'
		};

	date: Date = new Date();

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		public svgService: SVGCacheService,
		private tagService: TagService,
		private postService: PostService,
		private acf: AcfService,
		private loadPage: LoadPage,
		private title: Title,
		private meta: Meta,
		private seoService: SeoService
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });
	}

	ngOnInit() {
		this.route
			.params
			.mergeMap((res: Params) => {
				this.loadPage.set('true');

				return this.tagService.getTagByslug(res['tagSlug']);
			})
			.map((tag: Array<ITag>) => {
				if (tag.length < 0) {
					this.router.navigate(['/404'], { 'replaceUrl': true });
					return null;
				} else {
					return tag[0];
				}
			})
			.map((tag: ITag) => {
				this.acf
					.getTagACF(tag.id)
					.subscribe((res: any) => {
						const url: string = domain + this.router.url + '/';
						this.seoService.setSEOTags(res.acf, url);
					});

				return tag;
			})
			.subscribe(
				(tag: ITag) => {
					this.initPosts(tag);
				},
				(error) => {
					this.router.navigate(['/404'], { 'replaceUrl': true });
				}
			);
	}

	initPosts(tag: ITag): void {
		const query: string = '?tags=' + tag.id + '&per_page=10';
		this.postService
			.getPostQuery(query)
			.subscribe(
				(res: Array<IPost>) => {
					if (res.length > 0) {
						this.tag = tag;
						this.posts = res;
						this.loadPage.set('false');
					} else {
						this.router.navigate(['/404'], { 'replaceUrl': true });
					}
				},
				(error) => {
					this.router.navigate(['/404'], { 'replaceUrl': true });
				}
			);
	}

	ngOnDestroy() {
		this.loadPage.set('true');
	}

	dateChanged($event: Event): void {
		this.loadPage.set('true');
		const query: string = '?tag=' + this.tag.id + '&per_page=100&' + this.dateParams(this.date);
		this.postService
			.getPostQuery(query)
			.subscribe((res: Array<IPost>) => {
				this.posts = res;
				this.loadPage.set('false');
			});
	}

	dateParams(date: Date): string {
		const year: string = date.getFullYear().toString();
		const month: string = (date.getMonth() < 10) ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
		const day: string = (date.getDate() < 10) ? '0' + date.getDate().toString() : date.getDate().toString();

		return 'after=' + year + '-' + month + '-' + day + 'T00:00:00&before=' + year + '-' + month + '-' + day + 'T23:55:59';
	}

	sortPosts(a: IPosts, b: IPosts): number {
		if ((a.date.getTime() - b.date.getTime()) > 0) {
			return -1;
		} else if ((a.date.getTime() - b.date.getTime()) < 0) {
			return 1;
		}

		return 0;
	}

}
