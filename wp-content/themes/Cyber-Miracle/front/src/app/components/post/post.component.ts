import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { ScrollToService } from '../../service/scrollTo.service';
import { slideInLeftAnimation, slideFade } from '../../animation/animate';
import { PostService } from '../../service/posts.service';
import { IPost, IPosts, IUser } from '../../interface/interface';
import { UserService } from '../../service/user.service';
import { domain } from '../../options';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { LoggedInUserService, LoadPage } from '../../service/global-vars.service';
import { SeoService } from '../../service/seo.service';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/delay';

@Component({
	'selector': 'app-post',
	'templateUrl': './post.component.html',
	'styleUrls': ['./post.component.scss'],
	'animations': [slideFade]
})
export class PostComponent implements OnInit, OnDestroy {

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	category: string;
	post: IPost;
	author: IUser;
	innerHTML: any;
	recPosts: Array<Object>;

	// Путь до картинки которую необходимо открыть в модальном окне
	modalImageSrc = '';

	// Значение отображающее, что модальное окно открыто
	viewModal = false;

	/**
	 *   Часовой пояс
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-16
	 */
	timeZone: string = (new Date()).toString().split('GMT')[1].split(' (')[0];

	constructor(
		private route: ActivatedRoute,
		private postService: PostService,
		private userService: UserService,
		private scrollToService: ScrollToService,
		public domsanitizer: DomSanitizer,
		public loadPage: LoadPage,
		private router: Router,
		private seoService: SeoService
	) {}

	ngOnInit() {
		this.route
			.params
			.mergeMap((res: Params) => {
				return this.postService.getPostBySlug(res['postSlug']);
			})
			.subscribe(
				(res: Array<IPost>) => {
					if (res.length === 0) {
						this.router.navigate(['/404'], { 'replaceUrl': true });
					} else {
						this.post = res[0];
						const url: string = domain + this.router.url + '/';
						this.seoService.setSEOTags(this.post.acf, url);
						this.innerHTML = this.domsanitizer
											.bypassSecurityTrustHtml(
												this.post.content.rendered
											);
						this.getAuthor();
						this.loadPage.set('false');

						this.goToComments();
					}
				},
				(error) => {
					this.router.navigate(['/404'], { 'replaceUrl': true });
				}
			);

		this.postService
			.getRandomPosts()
			.subscribe((res: Array<Object>) => {
				this.recPosts = res;
			});
	}

	ngOnDestroy() {
		this.loadPage.set('true');
	}

	getAuthor(): void {
		this.userService
			.getUser(this.post.author)
			.subscribe((user: IUser) => {
				this.author = user;
			});
	}

	active(url: string = '/'): void {
		this.router
			.navigateByUrl(url)
			.then((e) => {
				this.postService
					.getRandomPosts()
					.subscribe((res: Array<Object>) => {
						this.recPosts = res;
					});
			});
	}

	goToComments(): void {
		this.route
			.fragment
			.delay(2000)
			.subscribe((res) => {
				if (res !== null) {
					this.scrollToService
						.scrollTo(
							'#' + res,
							500,
							50
						);
				}
			});
	}

	openModalImage($event) {
		if ($event.target.tagName.toLowerCase() === 'img') {
			this.modalImageSrc = $event.target.getAttribute('src');
			this.viewModal = true;
			console.log(this.viewModal);
		}
	}

	closeModal() {
		this.viewModal = false;
	}
}
