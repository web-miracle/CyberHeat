import { Component, HostBinding, OnInit } from '@angular/core';
import { slideInLeftAnimation } from '../../animation/animate';
import { StreamService } from '../../service/stream.service';
import { LoadPage } from '../../service/global-vars.service';
import { IStreams, IStream } from '../../interface/interface';

import { ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	'selector': 'app-single-stream',
	'templateUrl': './single-stream.component.html',
	'styleUrls': ['./single-stream.component.scss'],
	'animations': [ slideInLeftAnimation ]
})
export class SingleStreamComponent implements OnInit {

	channelName: string;
	iframe: any;

	@HostBinding('@routeAnimation') routeAnimation = true;
	@HostBinding('style.display') display = 'block';
	@HostBinding('style.position') position = 'absolute';
	@HostBinding('style.width') width = '100%';

	constructor(
		public loadPage: LoadPage,
		public domsanitizer: DomSanitizer,
		private router: ActivatedRoute
	) {}

	ngOnInit() {
		this.router
			.params
			.subscribe((res: Params) => {
				this.channelName = res.channel;
				this.iframe =
					this.domsanitizer
						.bypassSecurityTrustHtml(`<iframe
									src="http://player.twitch.tv/?channel=${this.channelName}"
									height="500"
									width="70%"
									frameborder="0"
									scrolling="no"
									allowfullscreen="true">
							  </iframe>
							  <iframe frameborder="0"
										scrolling="no"
										id="chat_embed"
										src="http://www.twitch.tv/embed/${this.channelName}/chat"
										height="500"
										width="29%">
							  </iframe>`);

				this.loadPage.set('false');
			});
	}
}
