import {
	animate,
	AnimationEntryMetadata,
	state,
	style,
	transition,
	trigger
} from '@angular/core';

const slideInLeftAnimation: AnimationEntryMetadata =
	trigger(
		'routeAnimation',
		[
			state(
				'*',
				style({
					'opacity': 1,
					'transform': 'translateX(0)'
				})
			),
			transition(
				':enter',
				[
					style({
						'opacity': 0
					}),
					animate('1s 1s ease-in')
				]
			),
			transition(
				':leave',
				[
					animate(
						'0.5s 1s ease-in',
						style({
							'opacity': 0,
							'transform': 'translateX(100%)'
						})
					)
				]
			)
		]
	);

const slideFade: AnimationEntryMetadata =
	trigger(
		'routeAnimation',
		[
			state(
				'*',
				style({
					'opacity': 1
				})
			),
			transition(
				':enter',
				[
					style({
						'opacity': 0
					}),
					animate('0.5s ease-in')
				]
			),
			transition(
				':leave',
				[
					animate(
						'0.5s ease-out',
						style({
							'opacity': 0
						})
					)
				]
			)
		]
	);

const streamAnimation: AnimationEntryMetadata =
	trigger(
		'streamAnimation',
		[
			state(
				'*',
				style({
					'opacity': 1
				})
			),
			transition(
				':enter',
				[
					style({
						'opacity': 0
					}),
					animate('500ms 550ms ease-in')
				]
			),
			transition(
				':leave',
				[
					animate(
						'500ms ease-out',
						style({
							'opacity': 0
						})
					)
				]
			)
		]
	);

const newsAnimation: AnimationEntryMetadata =
	trigger(
		'newsAnimation',
		[
			state(
				'*',
				style({
					'transform': 'translateX(0%)',
					'position': 'relative',
					'top': 0,
					'opacity': 1
				})
			),
			transition(
				':enter',
				[
					style({
						'opacity': 0,
						'position': 'absolute',
						'transform': 'translateX(-100%)'
					}),
					animate(
						'1ms 500ms',
						style({
							'position': 'relative'
						})
					),
					animate(
						'500ms cubic-bezier(1, 0, 0.9, 0.5)',
						style({
							'opacity': 1,
							'position': 'relative',
							'transform': 'translateX(0%)'
						})
					)
				]
			),
			transition(
				':leave',
				[
					style({
						'opacity': 1,
						'transform': 'translateX(0%)'
					}),
					animate(
						'500ms cubic-bezier(1, 0, 0.9, 0.5)',
						style({
							'opacity': 0,
							'transform': 'translateX(100%)'
						})
					)
				]
			)
		]
	);

export {
	slideInLeftAnimation,
	slideFade,
	streamAnimation,
	newsAnimation
};
