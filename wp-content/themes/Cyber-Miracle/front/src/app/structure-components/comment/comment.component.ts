import { Component, OnInit, OnDestroy, OnChanges, Input, Output } from '@angular/core';
import { CommentService } from '../../service/comment.service';
import { LoggedInUserService } from '../../service/global-vars.service';
import { IComment } from '../../interface/interface';

import { Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

@Component({
	'selector': 'app-comment',
	'templateUrl': './comment.component.html',
	'styleUrls': ['./comment.component.scss']
})
export class CommentComponent implements OnInit, OnDestroy, OnChanges {

	/**
	 *   Id поста на котором необходимо вывести комментарии
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-01
	 */
	@Input() postId: number;

	/**
	 *   Количество комментариев поста
	 */
	@Input() comment_count: number;

	intervalID: any;
	comments: Array<IComment> = [];
	loadCommentForm = false;

	subCommentId = 0;
	subCommentAuthor = '';

	result: Array<any>;
	commentFormOpen = false;

	constructor(
		private commentService: CommentService,
		public loggedUser: LoggedInUserService
	) {}

	ngOnInit() {
		this.initComment();
		this.setInterval();
	}

	ngOnDestroy() {
		this.clearInterval();
	}

	ngOnChanges(changes) {
		if (changes.postId && !changes.postId.firstChange) {
			this.ngOnDestroy();
		}

		if (changes.comment_count && !changes.comment_count.firstChange) {
			this.ngOnInit();
		}
	}

	setInterval() {
		const self = this;
		this.intervalID =
			setInterval(
				() => {
					const now: Date = new Date();
					now.setSeconds(now.getSeconds() - 20);
					self.addComment(now);
				},
				20000
			);
	}

	clearInterval() {
		clearInterval(this.intervalID);
	}

	initComment() {
		const query: string = 'post=' + this.postId + '&per_page=100&order=asc';
		this.commentService
			.getCommentQuery(query)
			.subscribe((res: Array<IComment>) => {
				this.result = this.sort(res);
			});
	}

	addComment(time: Date, afterSend: boolean = false) {
		const timeZone = 2;
		time.setHours(time.getHours() + timeZone);
		const dateISO = time.toISOString();

		const query: string = 'post=' + this.postId + '&per_page=100&after=' + dateISO;
		this.commentService
			.getCommentQuery(query)
			.mergeMap((res: Array<IComment>) => {
				return Observable.from(res);
			})
			.subscribe(
				(res: IComment) => {
					const comment: any = Object.assign({}, res);
					comment.childs = [];

					if (comment.parent === 0) {
						this.result = this.result.concat(comment);
					} else {
						this.concatComment(comment);
					}
				},
				(error) => {
					console.log(error);
				},
				() => {
					if (afterSend) {
						this.clearAnswer();
						this.setInterval();
					}
				}
			);
	}

	concatComment(comment, comments = this.result) {
		const iterations = comments.length;
		for (let i = 0; i < iterations; i++) {
			if (comments[i].id === comment.parent) {
				comments[i].childs = comments[i].childs.concat(comment);

				return true;
			} else {
				if (this.concatComment(comment, comments[i].childs)) {
					return true;
				}
			}
		}

		return false;
	}

	sendComment(event: Event, content) {
		event.preventDefault();
		this.clearInterval();

		const message = (this.subCommentAuthor) ? '<b>' + this.subCommentAuthor + '</b>, ' + content.value : content.value;
		this.loadCommentForm = true;
		this.commentService
			.setComment(message, this.postId, this.subCommentId)
			.subscribe(
				(success) => {
					const now = new Date();
					now.setTime(now.getTime() - 2000);
					this.addComment(now, true);
				},
				(error) => {
					console.log(error);
					this.setInterval();
				},
				() => {
					this.loadCommentForm = false;
				}
			);
		content.value = '';
	}

	subComment(event) {
		this.commentFormOpen = true;
		this.subCommentId = event.id;
		this.subCommentAuthor = event.userCommentName;
	}

	clearAnswer() {
		this.subCommentId = 0;
		this.subCommentAuthor = '';
	}

	sort(res) {
		const result = [];
		res.forEach((comment) => {
				if (comment.parent === 0) {
					comment.childs = this.childs(res, comment.id);
					result.push(comment);
				}
			});

		return result;
	}

	childs(res, id) {
		const result = [];

		res.forEach((comment) => {
				if (comment.parent === id) {
					comment.childs = this.childs(res, comment.id);
					result.push(comment);
				}
			});

		return result;
	}
}
