import { Component, Output, OnInit, Renderer2, EventEmitter } from '@angular/core';
import { WpService, WpUser } from 'ngx-wordpress';
import { GlobalConfigService } from './../../service/global-config.service';
import { IMenuItem, IMenuStore, IUser } from './../../interface/interface';
import { domain } from './../../options';
import { SVGCacheService } from 'ng-inline-svg';
import { LoggedInUserService } from './../../service/global-vars.service';

/**
 *   Корневой компонент приложения
 *   @autor   Mishalov_Pavel
 *   @version 0.1.5
 *   @date    2018-01-24
 */
@Component({
	'selector': 'app-navigation',
	'templateUrl': './navigation.component.html',
	'styleUrls': ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

	@Output() openModalEvent: EventEmitter<any> = new EventEmitter();

	/**
	 *   Массив пунктов меню приложения
	 *   @type {Array<IMenuItem>}
	 */
	menu: Array<IMenuItem>;

	/**
	 *   Свойство отображающее, что пользователь авторизован
	 *   @type {boolean}
	 */
	auth = false;
	username = 'username';

	/**
	 *   Разрешение экрана
	 *   @type {number}
	 */
	windowWidth: number;

	/**
	 *   Конструктор компонента
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 *   @param   {WpService}           public wpService    Сервис плагина ngx-wordpress
	 *   @param   {GlobalConfigService} public globalConfig Сервис глобальных настроек приложения
	 *   @param   {GameService}         public gameService  Сервис информации о турнирах и матчах
	 */
	constructor(
		public wpService: WpService,
		public globalConfig: GlobalConfigService,
		public svgService: SVGCacheService,
		public loggedUser: LoggedInUserService,
		private render2: Renderer2
	) {
		this.windowWidth = window.screen.width;

		render2.listen(
			'window',
			'resize',
			(e) => {
				this.windowWidth = window.screen.width;
			}
		);

		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });

		const authKey = localStorage.getItem('authKeys') || undefined;
		if (authKey) {
			/**
			 *   Метод авторизации пользователя, подробнее
			 *   https://github.com/MurhafSousli/ngx-wordpress/blob/master/src/service/authentication/auth.service.ts
			 */
			this.wpService
				.auth()
				.login(authKey, 'basic')
				.subscribe((user) => {
					if (user.code === 'incorrect_password') {
						this.wpService
							.auth()
							.logout();
						this.auth = false;
						this.loggedUser.set(null);
						localStorage.removeItem('authKeys');
					} else {
						this.username = user.name;
						this.loggedUser.set(user);
						this.auth = true;
					}
			});
		}
	}

	/**
	 *   При инициализации устанавливается логотип и меню приложения
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 */
	ngOnInit() {
		this.globalConfig
			.getMenu()
			.subscribe((res: Array<IMenuItem>) => {
				this.menu = res;
			});
	}

	/**
	 * [openModal description]
	 * @description открытие модального окна
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-01-30
	 * @version     0.1
	 * @param       {nodeElement}             modal объект модального окна
	 */
	openModal() {
		this.openModalEvent.emit(null);
	}

	/**
	 *   Метод чтобы разлогинить пользователь
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 */
	logout() {
		/**
		 *   Метод разлогинить пользователя, подробнее
		 *   https://github.com/MurhafSousli/ngx-wordpress/blob/master/src/service/authentication/auth.service.ts
		 */
		this.wpService
			.auth()
			.logout();
		this.auth = false;
		this.loggedUser.set(null);
		localStorage.removeItem('authKeys');
	}
}
