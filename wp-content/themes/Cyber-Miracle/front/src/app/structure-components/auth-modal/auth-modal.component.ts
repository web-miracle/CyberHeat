import { Component, Renderer2, Input, Output, EventEmitter } from '@angular/core';
import { WpService, WpUser } from 'ngx-wordpress';
import { ILogo, IMatch, IMatchMember, IUser } from './../../interface/interface';
import { domain } from './../../options';
import { trigger, style, animate, transition } from '@angular/animations';
import { SVGCacheService } from 'ng-inline-svg';
import { UserService } from './../../service/user.service';
import { LoggedInUserService } from './../../service/global-vars.service';

/**
 *   Корневой компонент приложения
 *   @autor   Mishalov_Pavel
 *   @version 0.1.5
 *   @date    2018-01-24
 */
@Component({
	'selector': 'app-auth-modal',
	'animations': [
		trigger(
			'enterAnimation',
			[
				transition(
					':enter',
					[
						style(
							{
								'transform': 'translateX(100%)',
								'opacity': 0
							}
						),
						animate(
							'500ms 300ms',
							style(
								{
									'transform': 'translateX(0)',
									'opacity': 1
								}
							)
						)
					]
				),
				transition(
					':leave',
					[
						style(
							{
								'position': 'absolute',
								'top': '0',
								'transform': 'scale(1)',
								'opacity': 1
							}
						),
						animate(
							'500ms',
							style(
								{
									'transform': 'scale(0)',
									'opacity': 0
								}
							)
						)
					]
				)
			]
		)
	],
	'templateUrl': './auth-modal.component.html',
	'styleUrls': ['./auth-modal.component.scss']
})
export class AuthModalComponent {

	@Input() logo: ILogo;

	@Output() closeModalEvent: EventEmitter<any> = new EventEmitter();

	/**
	 *   Свойство меняющее вкладку "регистрация/авторизация" в окне регистрации
	 *   true - регистрация
	 *   false - авторизация
	 *   @type {boolean}
	 */
	register = false;

	/**
	 *   Свойство меняющее иконку checkbox в форме
	 *   true - выбрано
	 *   false - не выбрано
	 *   @type {boolean}
	 */
	checked = true;

	/**
	 *   Свойство загрузки процесса
	 *   true - выбрано
	 *   false - не выбрано
	 *   @type {boolean}
	 */
	loadForm = 'false';

	/**
	 *   Конструктор компонента
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 *   @param   {WpService}           public wpService    Сервис плагина ngx-wordpress
	 *   @param   {GlobalConfigService} public globalConfig Сервис глобальных настроек приложения
	 */
	constructor(
		public wpService: WpService,
		public svgService: SVGCacheService,
		private render: Renderer2,
		public userservice: UserService,
		public loggedUser: LoggedInUserService
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });
	}

	/**
	 *   Метод, чтобы залогинить пользователя
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 */
	login(username: string, password: string, modal) {
		/**
		 *   Метод авторизации пользователя, подробнее
		 *   https://github.com/MurhafSousli/ngx-wordpress/blob/master/src/service/authentication/auth.service.ts
		 */
		this.wpService
			.auth()
			.basic(username, password, true)
			.subscribe((user) => {
				this.loadForm = 'false';
				if (user.id) {
					this.closeModal();
					this.loggedUser.set(user);
				} else if (user.code === 'incorrect_password') {
					modal.querySelector('.form__field-password')
						.classList
						.add('form__field_error');

					modal.querySelector('.form__field-password .form__field-message')
						.innerHTML = 'Неверный пароль';

					this.logout();
				} else if (user.code === 'invalid_username') {
					modal.querySelector('.form__field-login')
						.classList
						.add('form__field_error');

					modal.querySelector('.form__field-login .form__field-message')
						.innerHTML = 'Неверный логин';

					this.logout();
				} else if (user.code === 'empty_username') {
					modal.querySelector('.form__field-login')
						.classList
						.add('form__field_error');

					modal.querySelector('.form__field-login .form__field-message')
						.innerHTML = 'Заполните логин';

					this.logout();
				} else if (user.code === 'empty_password') {
					modal.querySelector('.form__field-password')
						.classList
						.add('form__field_error');

					modal.querySelector('.form__field-password .form__field-message')
						.innerHTML = 'Заполните пароль';

					this.logout();
				} else {
					this.logout();
				}
		});
	}

	/**
	 * [submitFormAuth description]
	 * @description обработчик сабмита формы авторизации: очистка ошибок, попытка сделать логин. TODO: встроить валидацию
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {event}	     	        event    [событие формы]
	 * @param       {nodeElement}           login    [инпут ввода логина]
	 * @param       {nodeElement}           password [инпут ввода пароля]
	 * @param       {nodeElement}			modal    [объект модального окна]
	 */
	submitFormAuth(event, login, password, modal) {
		event.preventDefault();
		// Остальная валидация ожидается тут
		modal.querySelectorAll('.form__field_error')
			.forEach((element) => {
				element.classList.remove('form__field_error');
			});

		this.loadForm = 'true';
		this.login(login.value, password.value, modal);
	}

	submitFormRegister(event, login, password, password2, checkbox, email, modal) {
		event.preventDefault();
		modal.querySelectorAll('.form__field_error')
			.forEach((element) => {
				element.classList.remove('form__field_error');
			});

		if (!checkbox.checked) {
			modal.querySelector('.form__field-checkbox')
				.classList
				.add('form__field_error');
		}
		if (password.value !== password2.value) {
			modal.querySelector('.form__field-password')
				.classList
				.add('form__field_error');

			modal.querySelector('.form__field-password2')
				.classList
				.add('form__field_error');

			modal.querySelector('.form__field-password .form__field-message')
				.innerHTML = 'Пароли не совпадают';
		}
		// Остальная валидация ожидается тут
		this.loadForm = 'true';
		this.registerUser(login, password, email, modal);
	}

	registerUser(login, password, email, modal) {
		this.userservice
			.signUpUser(login.value, password.value, email.value)
			.subscribe(
				(user: IUser) => {
					if (user.id) {
						this.wpService
							.auth()
							.basic(login.value, password.value, true)
							.subscribe((res: IUser) => {
								this.loadForm = 'false';
								this.closeModal();
								this.loggedUser.set(res);
							});
					}
				},
				(error) => {
					this.loadForm = 'false';
					switch (error.json().code) {
						case 'existing_user_login' : {
							modal.querySelector('.form__field-login')
								.classList
								.add('form__field_error');

							modal.querySelector('.form__field-login .form__field-message')
								.innerHTML = 'Пользователь с таким логином уже есть в системе';
							break;
						}
						case 'existing_user_email' : {
							modal.querySelector('.form__field-email')
								.classList
								.add('form__field_error');

							modal.querySelector('.form__field-email .form__field-message')
								.innerHTML = 'Пользователь с таким e-mail уже есть в системе';
							break;
						}
					}
				}
			);
	}

	/**
	 * [closeModal description]
	 * @description закрытие модального окна
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {nodeElement}             modal объект модального окна
	 */
	closeModal() {
		this.closeModalEvent.emit(null);
	}

	/**
	 *   Метод чтобы разлогинить пользователь
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 */
	logout() {
		/**
		 *   Метод разлогинить пользователя, подробнее
		 *   https://github.com/MurhafSousli/ngx-wordpress/blob/master/src/service/authentication/auth.service.ts
		 */
		this.wpService
			.auth()
			.logout();

		this.loggedUser.set(null);
		localStorage.removeItem('authKeys');
	}

	/**
	 * [toggleCheck description]
	 * @description смена изображения чекбокса в форме авторизации/логина
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 */
	toggleCheck() {
		if (this.checked) {
			this.render
				.addClass(
					document.querySelector('.fa-check-square'),
					'fa-square'
				);

			this.render
				.removeClass(
					document.querySelector('.fa-check-square'),
					'fa-check-square'
				);

		} else {
			this.render
				.addClass(
					document.querySelector('.fa-square'),
					'fa-check-square'
				);

			this.render
				.removeClass(
					document.querySelector('.fa-square'),
					'fa-square'
				);

		}

		this.checked = !this.checked;
	}

	/**
	 * [onFocus description]
	 * @description добавление класса к родителю инпута при фокусе инпута. на класс повешены эффект гуглМатериал
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {nodeElement}             element элемент в котором происходит фокус
	 * @param       {nodeElement}             parent  родитель элемента
	 */
	onFocus(element, parent) {
		parent.classList.add('form__field_focus');
	}

	/**
	 * [onBlur description]
	 * @description удаление класса у родителя инпута при потере фокуса
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {nodeElement}             element элемент, в котором происходит потеря фокуса
	 * @param       {nodeElement}             parent  родитель инпута
	 */
	onBlur(element, parent) {
		if (element.value === '') {
			parent.classList.remove('form__field_focus');
		} else {
			parent.classList.add('form__field_focus');
		}
	}

	/**
	 * [onInput description]
	 * @description добавление класса фокуса при вводе текста в инпут
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {nodeElement}             element инпут, в котором вводим
	 * @param       {nodeElement}             parent  родитель инпута, для которого добавляем
	 */
	onInput(element, parent) {
		if (element.value !== '') {
			parent.classList.add('form__field_focus');
		} else {
			parent.classList.remove('form__field_focus');
		}
	}
}
