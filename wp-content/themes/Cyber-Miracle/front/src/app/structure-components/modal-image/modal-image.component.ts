import { Component, HostBinding, HostListener, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { slideInLeftAnimation, newsAnimation } from '../../animation/animate';

@Component({
	'selector': 'app-modal-image',
	'templateUrl': './modal-image.component.html',
	'styleUrls': ['./modal-image.component.scss']
})
export class ModalImageComponent {

	/**
	 *   Id поста на котором необходимо вывести комментарии
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-01
	 */
	@Input() image: string;

	/**
	 *   Количество комментариев поста
	 */
	@Input() show;

	@Output() closeModal: EventEmitter<any> = new EventEmitter();

	@HostBinding('style.opacity') get getDisplay() {
		return this.show ? 1 : 0;
	}

	@HostBinding('style.transform') get getTransform() {
		return this.show ? 'translateY(0)' : 'translateY(100%)';
	}

	@HostListener('click', ['$event.target.tagName']) close(tag: string) {
		if (tag.toLowerCase() !== 'img') {
			this.hide();
		}
	}

	constructor() {}

	hide() {
		this.closeModal.emit(null);
	}
}
