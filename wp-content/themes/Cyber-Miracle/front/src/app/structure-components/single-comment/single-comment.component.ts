import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommentService } from '../../service/comment.service';
import { IComment, IUser } from '../../interface/interface';
import { UserService } from '../../service/user.service';

@Component({
	'selector': 'app-single-comment',
	'templateUrl': './single-comment.component.html',
	'styleUrls': ['./single-comment.component.scss']
})
export class SingleCommentComponent {

	@Input() parentComment: number;
	@Input() postID: number;
	@Input() commentID: number;
	@Input() commentAuthorAvatarUrl: string;
	@Input() authorName: string;
	@Input() commentDate: string;
	@Input() commentContent: string;

	@Input() childs: any;

	@Output() answer: EventEmitter<any> = new EventEmitter();

	comments: Array<IComment>;
	author: IUser;
	intervalID: any;

	constructor(
		private commentService: CommentService,
		private userService: UserService
	) {}

	getUser(author: number = 0) {
		this.userService
			.getUser(author)
			.subscribe((res: IUser) => {
				this.author = res;
			});
	}

	commentAnswer(id: number = this.commentID, user: string = this.authorName) {
		this.answer
			.emit({
				'id': id,
				'userCommentName': user
			});
	}

	subCommentAnswer(event) {
		this.commentAnswer(event.id, event.userCommentName);
	}
}
