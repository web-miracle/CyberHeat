import { Component, Renderer2, OnInit } from '@angular/core';
import { GlobalConfigService } from './service/global-config.service';
import { GameService } from './service/game.service';
import { ILogo, IMatch, IMatchMember } from './interface/interface';
import { domain } from './options';
import { SVGCacheService } from 'ng-inline-svg';
import { LoadPage } from './service/global-vars.service';

/**
 *   Корневой компонент приложения
 *   @autor   Mishalov_Pavel
 *   @version 0.1.5
 *   @date    2018-01-24
 */
@Component({
	'selector': 'app-root',
	'templateUrl': './app.component.html',
	'styleUrls': ['./app.component.scss']
})
export class AppComponent implements OnInit {

	/**
	 *   Логотип приложения
	 *   @type {ILogo}
	 */
	logo: ILogo =
		{
			'time': 0,
			'url': '',
			'alt': '',
			'title': ''
		};

	/**
	 *   Массив матчей
	 *   @type {Array<IMatch>}
	 */
	games: Array<IMatch>;

	/**
	 *   Конструктор компонента
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 *   @param   {GlobalConfigService} public globalConfig Сервис глобальных настроек приложения
	 *   @param   {GameService}         public gameService  Сервис информации о турнирах и матчах
	 */
	constructor(
		public globalConfig: GlobalConfigService,
		public gameService: GameService,
		public svgService: SVGCacheService,
		public loadPage: LoadPage,
		private render: Renderer2
	) {
		const path: string = domain + '/wp-content/themes/Cyber-Miracle/front/src/assets/';
		svgService.setBaseUrl({ 'baseUrl': path });
	}

	/**
	 *   При инициализации устанавливается логотип и меню приложения
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-01-24
	 */
	ngOnInit() {
		this.globalConfig
			.getLogo()
			.subscribe((res: ILogo) => {
				this.logo = res;
			});

		this.games = this.gameService.getLastGame();
	}

	/**
	 * [openModal description]
	 * @description открытие модального окна
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-01-30
	 * @version     0.1
	 * @param       {nodeElement}             modal объект модального окна
	 */
	openModal(modal) {
		this.render.addClass(document.body, 'modal--active');
		this.render.addClass(modal, 'modal__shown');
	}

	/**
	 * [closeModal description]
	 * @description закрытие модального окна
	 * @author Dmitriy Novikov
	 * @email       dnovikov33@gmail.com
	 * @date        2018-02-02
	 * @version     0.1
	 * @param       {nodeElement}             modal объект модального окна
	 */
	closeModal(modal) {
		this.render.removeClass(document.body, 'modal--active');
		this.render.removeClass(modal, 'modal__shown');
	}
}
