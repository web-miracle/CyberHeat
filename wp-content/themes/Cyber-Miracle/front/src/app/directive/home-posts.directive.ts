import { Directive, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';

/**
 *   Директива для подгрузки новых постов на главной странице
 *   @autor   Mishalov_Pavel
 *   @version 1.1
 *   @date    2018-01-24
 */
@Directive({
	'selector': '[appMiracleHomePosts]'
})
export class HomePostsDirective {

	/**
	 *   Свойство для работы с DOM
	 *   подробнее:
	 *   	https://angular.io/api/core/Renderer2
	 *   @type {Renderer2}
	 */
	render: Renderer2;

	/**
	 *   Событие, которое испускает директива в случае необходимости подгрузки новых постов
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @param   {EventEmitter}    sendForNews       Событие, на которое необходимо повесить обработчик
	 *                                                подробнее:
	 *                                                	https://angular.io/api/core/EventEmitter
	 */
	@Output() sendForNews: EventEmitter<any> = new EventEmitter();

	/**
	 *   Элемент, на котором проинициализирована директива
	 *   подробнее https://angular.io/guide/attribute-directives#respond-to-user-initiated-events
	 *
	 *   @type {ElementRef}
	 */
	elem: ElementRef;

	/**
	 *   При инициализации вешает слушатель на скролл документа и
	 *   в случае если до конца скролла остается около 200px испускает событие `sendForNews`
	 *
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-02-08
	 *   @param   {Renderer2}    private render2  Класс для работы с DOM
	 *                                   			подробнее: https://angular.io/api/core/Renderer2
	 *   @param   {ElementRef}   private el       Элемент, на котором проинициализирована директива
	 *                                   			подробнее: https://angular.io/guide/attribute-directives#respond-to-user-initiated-events
	 */
	constructor(
		private render2: Renderer2,
		private el: ElementRef
	) {
		render2.listen('window', 'scroll', (event) => {

				// Высота от начала документа до проинициализированого элемента
				const elementTop: number = el.nativeElement.offsetTop;

				// Высота проинициализированого элемента
				const elementHeight: number = el.nativeElement.offsetHeight;

				// Растояние, которое проскролли документа от начала
				const scrollTop: number = window.pageYOffset || document.documentElement.scrollTop;

				// Высота рабочего окна браузера
				const screenH: number = window.screen.availHeight;

				if (elementTop + elementHeight < scrollTop + screenH + 200) {
					this.sendForNews.emit(null);
				}
			});
	}
}
