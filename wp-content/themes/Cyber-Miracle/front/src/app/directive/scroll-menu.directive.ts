import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
	'selector': '[appScrollMenu]'
})
export class ScrollMenuDirective implements OnInit {

	render: Renderer2;
	element: ElementRef;
	scrollTop: number;
	top = 0;

	constructor(
		private render2: Renderer2,
		private elem: ElementRef
	) {
		this.scrollTop = window.pageYOffset || document.documentElement.scrollTop;

		this.element = elem;
		this.render = render2;

		this.top = this.element.nativeElement.offsetHeight;

		this.element.nativeElement.style.top = this.top + 'px';

		this.render
			.listen('window', 'scroll', (event) => {
				this.toggleScroll();
			});
	}

	ngOnInit() {
		this.toggleScroll();
	}

	toggleScroll() {
		const elementTop: number = this.element.nativeElement.offsetTop;
		const scrollTop: number = window.pageYOffset || document.documentElement.scrollTop;

		if (elementTop + 200 <= scrollTop) {
			this.element.nativeElement.classList.add('header__navigation_scroll');

			if (this.scrollTop > scrollTop) {
				const toggle: number = this.scrollTop - scrollTop;
				this.top += (toggle / 2);

				if (this.top > 0) {
					this.top = 0;
				}

				this.element.nativeElement.style.top = this.top + 'px';
			} else {
				const toggle: number = scrollTop - this.scrollTop;
				this.top -= (toggle / 2);

				if (this.top < (-this.element.nativeElement.offsetHeight)) {
					this.top = -this.element.nativeElement.offsetHeight;
				}

				this.element.nativeElement.style.top = this.top + 'px';
			}

			this.scrollTop = scrollTop;

		} else {
			this.element.nativeElement.classList.remove('header__navigation_scroll');
			this.element.nativeElement.style.top = '';
		}
	}
}
