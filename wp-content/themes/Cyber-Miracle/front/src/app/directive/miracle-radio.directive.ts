import { Directive, ElementRef, Renderer2, Input, OnChanges, AfterContentInit } from '@angular/core';

/**
 *   Директива для выстовления высоты в зависимости от ширины
 *   @autor   Mishalov_Pavel
 *   @version 1.1
 *   @date    2018-01-24
 */
@Directive({
	'selector': '[appMiracleRadio]'
})
export class MiracleRadioDirective implements OnChanges, AfterContentInit {

	/**
	 *   Пропорция, которую необходимо соблюдать для ширины к высоте
	 */
	@Input() radio: number;

	/**
	 *   Пропорция, которую необходимо соблюдать для ширины к высоте на разрешение менее 800px
	 */
	@Input() radioMobile: number|boolean = false;

	/**
	 *   Пропорция, которую необходимо соблюдать для ширины к высоте на разрешение менее 650px
	 */
	@Input() radioMobileL: number|boolean = false;

	/**
	 *   Значение для ширины, не обязательный параметр
	 */
	@Input() width = '';

	/**
	 *   Элемент, на котором проинициализирована директива
	 *   подробнее https://angular.io/guide/attribute-directives#respond-to-user-initiated-events
	 *
	 *   @type {ElementRef}
	 */
	elem: ElementRef;

	/**
	 *   Определяет экран
	 */

	windowWidth: number;

	constructor(
		private el: ElementRef,
		private render2: Renderer2
	) {
		this.elem = el;
	}

	ngAfterContentInit() {

		this.windowWidth = window.screen.width;

		if (this.width !== '') {
			this.elem.nativeElement.style.width = this.width;
		}

		let height = (this.windowWidth < 800 && this.radioMobile)
				?
			(this.elem.nativeElement.offsetWidth / Number(this.radioMobile.toString())) + 'px'
				:
			(this.elem.nativeElement.offsetWidth / this.radio) + 'px';

		height = (this.windowWidth <= 650 && this.radioMobileL)
				?
			(this.elem.nativeElement.offsetWidth / Number(this.radioMobileL.toString())) + 'px'
				:
			height;

		this.elem.nativeElement.style.height = height;

		this.render2
			.listen('window', 'resize', (e) => {
				this.ngOnChanges();
				this.windowWidth = window.screen.width;
			});
	}

	ngOnChanges() {
		if (this.width !== '') {
			this.elem.nativeElement.style.width = this.width;
		}
		let height = (this.windowWidth < 800 && this.radioMobile)
				?
			(this.elem.nativeElement.offsetWidth / Number(this.radioMobile.toString())) + 'px'
				:
			(this.elem.nativeElement.offsetWidth / this.radio) + 'px';

		height = (this.windowWidth <= 650 && this.radioMobileL)
				?
			(this.elem.nativeElement.offsetWidth / Number(this.radioMobileL.toString())) + 'px'
				:
			height;

		this.elem.nativeElement.style.height = height;
	}
}
