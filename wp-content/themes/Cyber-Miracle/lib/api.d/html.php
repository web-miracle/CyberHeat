<?php

add_action( 'rest_api_init', function () {
	register_rest_route( 'miracle/api', '/logo', array(
		'methods' => 'GET',
		'callback' => 'getLogo',
	));
	register_rest_route( 'miracle/api', '/menu', array(
		'methods' => 'GET',
		'callback' => 'getMenu',
	));
	register_rest_route( 'miracle/api', '/primary-post', array(
		'methods' => 'GET',
		'callback' => 'getPrimaryPost',
	));
	register_rest_route( 'miracle/api', '/online-streams', array(
		'methods' => 'GET',
		'callback' => 'getOnlineStreams',
	));
	register_rest_route( 'miracle/api', '/vk-posting', array(
		'methods' => 'GET',
		'callback' => 'vkAutoPosting',
	));
	register_rest_route( 'miracle/api', '/rand-posts', array(
		'methods' => 'GET',
		'callback' => 'getRandomPosts',
	));
});

function getLogo(){
	$logo = get_field( 'logo', 'option' );
	return $logo;
}

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Главное меню' );
}

function getMenu(){
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object( $locations['primary'] );
	return wp_get_nav_menu_items( $menu );
}

function getPrimaryPost(){

	$page = get_page_by_path( 'main' );

	$posts = get_field( 'горячие', $page->ID );

	foreach( $posts as $key => $post ){
		if( $key < 6 ){
			$post->{'miracle-post-primary-tag'} = get_field( 'miracle-post-primary-tag', $post->ID );
			$post->{'miracle-post-image'} = get_field( 'miracle-post-image', $post->ID );
			$post->{'miracle-post-category'} = get_field( 'miracle-post-category', $post->ID );
		}
	}

	return $posts;
}

function getOnlineStreams(){
	$streams = get_field('miracle-streams', 'option');
	$answer = array();

	foreach ($streams as $stream):
		if( $stream['status'] == 'enable' ):
			$answer[] = $stream['id'];
		endif;
	endforeach;

	return $answer;
}

function getRandomPosts(){
	$args = array(
			'numberposts' => 3,
			'orderby' => 'rand'
		);

	$posts = get_posts( $args );

	foreach ($posts as $post):
		$post->{'miracle-post-primary-tag'} = get_field( 'miracle-post-primary-tag', $post->ID );
		$post->{'miracle-post-image'} = get_field( 'miracle-post-image', $post->ID );
		$post->{'miracle-post-category'} = get_field( 'miracle-post-category', $post->ID );
	endforeach;

	return $posts;
}


function filter_post_json( $data, $post, $context ) {
	$data->data['comment_count'] = intval( get_comments_number( $post ) );
	
	return $data;
}
add_filter( 'rest_prepare_post', 'filter_post_json', 10, 3 );

function filter_comment_json( $data, $comment, $context ) {
	$author = $data->data['author'];
	$user_avatar = get_field( 'user_avatar', get_userdata($author) );

	if( $user_avatar ){
		$data->data['custom_author_avatar_urls'] = $user_avatar['sizes']['thumbnail'];
	}
	
	return $data;
}
add_filter( 'rest_prepare_comment', 'filter_comment_json', 10, 3 );




function vkAutoPosting(){
	if( !isset($_GET['url']) || empty($_GET['url']) )
		return array( false );

	$answer = wp_remote_get($_GET['url']);

	if( !is_array($answer) )
		return array( false );

	return $answer['body'];
}