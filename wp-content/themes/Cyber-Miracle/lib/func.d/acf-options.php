<?php

add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page(
        'Настройки организации',
        'Настройки организации',
        'manage_options',
        'miracle-company-settings',
        'miracle_custom_menu_page',
        'dashicons-id-alt',
        2
    );
}

function miracle_custom_menu_page(){
    ?>
    <h2>Общая информация организации</h2>
    <p>В данном разделе настраиваются общие данные организации, такие как:<p>
    <ul>
        <li><a href="?page=miracle-option-contacts">Логотип</a>,</li>
        <li><a href="?page=miracle-option-streams">Стримы</a></li>
    </ul>
    <?php
}


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Логотип',
        'menu_title'    => 'Логотип',
        'menu_slug'     => 'miracle-option-contacts',
        'parent_slug'   => 'miracle-company-settings',
    ));

    acf_add_options_page(array(
        'page_title'    => 'Стримы',
        'menu_title'    => 'Стримы',
        'menu_slug'     => 'miracle-option-streams',
        'parent_slug'   => 'miracle-company-settings',
    ));

    acf_add_options_page(array(
        'page_title'    => 'Настройка постинга в vk',
        'menu_title'    => 'VK autopost',
        'menu_slug'     => 'miracle-option-vk-posting',
        'parent_slug'   => '',
    ));
}