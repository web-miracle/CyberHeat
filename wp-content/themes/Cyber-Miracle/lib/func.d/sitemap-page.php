<?php

add_action( 'admin_menu', 'register_page_for_sitemap' );

function register_page_for_sitemap(){
    add_menu_page(
        'Sitemap generation',
        'Sitemap',
        'manage_options',
        'miracle-sitemap',
        'miracle_sitemap_page',
        'dashicons-id-alt',
        101
    );
}

function miracle_sitemap_page(){
    ?>
    <div style="clear: both; width: 100%; height: 50px"></div>
    <p>
        На данной странице Вы можете сгенерировать sitemap для поисковых роботов,<br>
        желательно его генерировать после каждого добавления новых страниц или записей<br>
        на сайте, но не является обязательным
    </p>
    <button id="sitemap-generation">Генерировать</button>

<style type="text/css">
    #sitemap-generation{
        padding: 0.5em 2em;
        background: #0074d9;
        border: none;
        border-radius: 1em;
        color: white;
        font-size: 16px;
        cursor: pointer;
        outline: none !important;
    }
    #sitemap-generation.load{
        opacity: 0.4;
        cursor: default;
        background: #ff4136;
    }
</style>
<script type="text/javascript">

    var button = document.getElementById('sitemap-generation');
    button.addEventListener('click', genSitemap);

    function genSitemap(){

        button.classList.add('load');
        button.innerText = 'Загружается';
        var obj = {
                'action': 'genSitemap'
            }

        jQuery.post( ajaxurl, obj, function(response) {
            console.log(response);
            button.classList.remove('load');
            button.innerText = 'Генерировать';
        });

    }
</script>
    <?php
}

add_action('wp_ajax_genSitemap', 'genSitemap');
function genSitemap() {

    $post_list = '';
    $cat_list = '';
    $tag_list = '';
    $primary_list = '';

    $primary = array( '/', '/news' );

    foreach( $primary as $value ):
        $primary_list .= "<url><loc>http://". $_SERVER['HTTP_HOST'] . $value ."</loc></url>";
    endforeach;

    $posts = get_posts(
                    array(
                            'numberposts'=> -1
                        )
                );

    foreach( $posts as $post ):
        $post_list .= "<url><loc>http://". $_SERVER['HTTP_HOST'] . "/news/" . get_field('miracle-post-category', $post)->slug. '/' . $post->post_name 
."</loc></url>";
    endforeach;

    $categories = get_categories(
                    array(
                            'type' => 'post',
                            'hide_empty'   => 1,
                        )
                );

    foreach( $categories as $category ):
        $cat_list .= "<url><loc>http://". $_SERVER['HTTP_HOST'] . "/tag/" . $category->slug ."</loc></url>";
    endforeach;

    $tags = get_tags();

    foreach( $tags as $tag ):
        $tag_list .= "<url><loc>http://". $_SERVER['HTTP_HOST'] . "/news/" . $tag->slug ."</loc></url>";
    endforeach;

    $list = $primary_list . $categories_list . $tags_list . $post_list;

    $result = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . $list . '</urlset>';

    $f = fopen(ABSPATH . 'sitemap.xml', 'w');
    fwrite($f, $result);
    fclose($f);

    wp_die();
}
