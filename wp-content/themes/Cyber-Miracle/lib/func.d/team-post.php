<?php

add_action( 'init', 'team_post_init' );

/**
 *   Инициализация поста команды
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2017-12-29
 *   @return  void         Регистрирует кастомные посты команд
 */
function team_post_init() {

	$labels = array(
		'name'                 => 'Команды',
		'singular_name'        => 'Команда',
		'menu_name'            => 'Команды',
		'name_admin_bar'       => 'Команды',
		'add_new'              => 'Добавить команду',
		'add_new_item'         => 'Добавить новую команду',
		'new_item'             => 'Новая команда',
		'edit_item'            => 'Редактировать команду',
		'view_item'            => 'Посмотреть команду',
		'all_items'            => 'Все команды',
		'search_items'         => 'Поиск по командам',
		'parent_item_colon'    => 'Родительский команда',
		'not_found'            => 'Нет результатов',
		'not_found_in_trash'   => 'Ничего не найдено в корзине',
		'attributes'           => 'Page Attributes',

		'featured_image' => 'Логотип команды',
		'set_featured_image' => 'Установить логотип',
		'remove_featured_image' => 'Убрать логотип',
		'use_featured_image' => 'Использовать как логотип',
	);

	$args = array(
		'labels'               => $labels,
		'description'          => '',
		'public'               => true,
		'exclude_from_search'  => false,
		'publicly_queryable'   => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_in_admin_bar'    => true,
//		'menu_position'        => 2,
		'menu_icon'            => 'dashicons-admin-page',
		'capability_type'      => 'post',
		'has_archive'          => true,
		'hierarchical'         => false,
		'supports'             => array( 'title', 'author', 'editor', 'thumbnail', 'excerpt', 'revisions' ),
//		'taxonomies'           => array( 'develop_cat' )
	);

	register_post_type( 'team', $args );
//	add_theme_support( 'post-thumbnails', array( 'post', 'portfolio' ) );

}
