<?php


add_action( 'admin_menu', 'register_page_for_twitch' );

function register_page_for_twitch(){
    add_menu_page(
        'Id канала twitch',
        'Twitch',
        'manage_options',
        'miracle-twitch-channale-id',
        'miracle_twitch_page',
        'dashicons-id-alt',
        100
    );
}

function miracle_twitch_page(){
    ?>
    <div style="clear: both; width: 100%; height: 50px"></div>
<label>
    Слаг канала: 
    <input id="twitch-slug" type="text" name="twitch-slug"><button id="twitch-search">Найти</button>
</label>
<ul id="channels"></ul>
<script type="text/javascript">
    var input = document.getElementById('twitch-slug');
    var button = document.getElementById('twitch-search');

    button.addEventListener( 'click', function(e){
        var slug = input.value;
        var channelList = document.getElementById('channels');
        var xhr = fetch( 'https://api.twitch.tv/kraken/search/channels?query=' + slug, {
            'method': 'GET',
            'headers': {
                    'Accept': 'application/vnd.twitchtv.v5+json',
                    'Client-ID': '0vosrgp8ysse6in0akwicn3q020ul2'
                }
        });

        channelList.innerHTML = '';

        xhr.then( function(res){
            return res.json();
        }).then( function(res){
            var channels = res.channels;
            var fragment = document.createDocumentFragment();
            res.channels.forEach( function(channel){
                var li = document.createElement('li');
                li.innerHTML = '<button style="margin-right: 10px" onclick="addChannel(this)">Добавить</button><a href="' + channel['url'] + '" target="_blank">' + channel['url'] + '</a><input type="hidden" name="id" value="' + channel['_id'] + '"><input type="hidden" name="channel" value="' + channel['display_name'] + '"><input type="hidden" name="link" value="' + channel['url'] + '"><input type="hidden" name="game" value="' + channel['game'] + '">';
                fragment.appendChild(li);
            });
            channelList.appendChild(fragment);
        });
    });

    function addChannel(node){
        var stream = node.parentNode;

        var id = stream.querySelector('[name="id"]').value;
        var display_name = stream.querySelector('[name="channel"]').value;
        var link = stream.querySelector('[name="link"]').value;
        var game = stream.querySelector('[name="game"]').value;

        var obj = {
                'action': 'addStream',
                'id': id,
                'channel': display_name,
                'link': link,
                'game': game
            }
        stream.parentNode.innerHTML = '';

        jQuery.post( ajaxurl, obj, function(response) {});

    }
</script>
    <?php
}

add_action('wp_ajax_addStream', 'addStream');
function addStream() {

    $streams = get_field( 'miracle-streams', 'option');
    $streams[] = array(
                    'id' => $_POST['id'],
                    'channel' => $_POST['channel'],
                    'link' => $_POST['link'],
                    'game' => $_POST['game'],
                    'status' => 'enable'
                );

    update_field( 'miracle-streams', $streams, 'option' );

    wp_die();
}