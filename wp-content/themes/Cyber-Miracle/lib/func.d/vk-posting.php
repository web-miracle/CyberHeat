<?php

add_action( 'save_post', 'send_post_in_vk', 10, 3 );

function send_post_in_vk( $post_ID, $post, $update ) {
	if( !$update )
		return;

	if ( wp_is_post_revision( $post_ID ) )
		return;

	$post_type = get_post_type($post_ID);

	if( $post_type !== 'post' )
		return;

	$cat = (get_field( 'miracle-post-category', $post_ID )) ? get_field( 'miracle-post-category', $post_ID )->slug : '';

	$description = get_the_title( $post_ID );
	$attachment = ( isset($_POST['post_name']) && !empty($_POST['post_name']) ) ?get_option( 'siteurl' ) . '/news/' . $cat . '/' . $_POST['post_name'] : '';
	
	if( get_field( 'vk-group-id', 'option' ) && get_field( 'vk-access_token', 'option' ) && isset($_POST['post_name']) && !empty($_POST['post_name']) && get_field( 'vk-post-publish', $post ) ):
		$url = 'https://api.vk.com/method/wall.post?owner_id=-' . get_field( 'vk-group-id', 'option' ) . '&from_group=1&message=' . $description . '&attachments=' . $attachment . '&guid=postID-' . $post_ID . '&v=5.73&access_token=' . get_field( 'vk-access_token', 'option' );
		$answer = wp_remote_get( $url );
		if( is_array($answer) ){
			file_put_contents( get_template_directory() . '/log.txt', $answer['body'] );
		}else{
			file_put_contents( get_template_directory() . '/log.txt', serialize($answer) );
		}

		update_field( 'vk-post-publish', false, $post );
	endif;
}