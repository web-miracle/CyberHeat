<?php

add_action('wp_enqueue_scripts', 'miracle_init_css');
function miracle_init_css() {
	wp_register_style('app', get_template_directory_uri().'/dist/style.css', null, null, 'all');
	wp_enqueue_style('app');
}

add_action('wp_enqueue_scripts', 'miracle_init_js');
function miracle_init_js() {
	wp_deregister_script( 'jquery' );
	if ( true ){
		$script_name = 'index-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/index.miracle.js', false, null, true);
	}

	if( isset( $script_name ) && !empty( $script_name ) ){
		wp_enqueue_script( $script_name );
		wp_localize_script( $script_name, 'ajax',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('Rosart')
			)
		);
	}

}

foreach(glob(get_stylesheet_directory() . '/lib/func.d/*.php') as $file) {
	include_once $file;
}

foreach(glob(get_stylesheet_directory() . '/lib/ajax.d/*.php') as $file) {
	include_once $file;
}

foreach(glob(get_stylesheet_directory() . '/lib/api.d/*.php') as $file) {
	include_once $file;
}
