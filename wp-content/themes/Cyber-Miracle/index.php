<?php http_response_code(200); ?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>CyberHeat</title>
		<meta name="description" content="Сайт аналитики киберспорта">
		<base href="/">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<meta name="fragment" content="!">
		<link rel="stylesheet"
			  href="<?= get_template_directory_uri() ?>/front/view/styles.css">
	</head>
	<body>
		<app-root></app-root>

<script defer="defer"
		src="https://use.fontawesome.com/releases/v5.0.6/js/all.js">
</script>

<script src="<?= get_template_directory_uri() ?>/front/view/inline.js"></script>
<script src="<?= get_template_directory_uri() ?>/front/view/polyfills.js"></script>
<script src="<?= get_template_directory_uri() ?>/front/view/main.js"></script>

	</body>
</html>
