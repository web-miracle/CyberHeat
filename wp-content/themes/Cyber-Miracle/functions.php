<?php

include('lib/init.php');

add_theme_support( 'custom-logo' );
define('FS_METHOD', 'direct');

add_action( 'admin_enqueue_scripts', 'admin_post_new_scripts' );
function admin_post_new_scripts($hook){
	if ( 'post-new.php' != $hook ) {
		return;
	}
	wp_enqueue_script( 'post_new_script', get_template_directory_uri() . '/admin/js/post-new.js', array(), '1.0' );
}

add_action( 'admin_enqueue_scripts', 'admin_post_scripts' );
function admin_post_scripts($hook){
	if ( 'post-new.php' != $hook ) {
		return;
	}
	wp_register_style( 'post_new_css', get_template_directory_uri() . '/admin/css/post-new.css', false, '1.0.0' );
	wp_enqueue_style( 'post_new_css' );
	?>
	<script>

		window.addEventListener( 'load', function(){

			var postSlug = document.getElementById('post_name').value;
			var li = document.getElementById('wp-admin-bar-view');
			li.innerHTML = '<a class="ab-item" href="' + document.location.origin +'/news/<?= get_the_category( $_GET['post'] )[0]->slug ?>/' + postSlug + '/" target="_blank">Просмотреть запись</a>';
			// var slug = document.querySelector('#editable-post-name');
			// console.log(slug);
			// slug.innerText = "news/<?= get_the_category( $_GET['post'] )[0]->slug ?>/" + postSlug;
		});
	</script>

<?php
}


///////////////////
// Settings Page //
///////////////////

function my_myme_types($mime_types){
	$mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
	return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

function darken_color($rgb, $darker=2) {

	$hash = (strpos($rgb, '#') !== false) ? '#' : '';
	$rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
	if(strlen($rgb) != 6) return $hash.'000000';
	$darker = ($darker > 1) ? $darker : 1;

	list($R16,$G16,$B16) = str_split($rgb,2);

	$R = sprintf("%02X", floor(hexdec($R16)/$darker));
	$G = sprintf("%02X", floor(hexdec($G16)/$darker));
	$B = sprintf("%02X", floor(hexdec($B16)/$darker));

	return $hash.$R.$G.$B;
}

/**
 * Deregister embet js
 */
function my_deregister_scripts(){
 wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

/* --------------------------------------------------------------------------
 * Отключаем Emojii
 * -------------------------------------------------------------------------- */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
add_filter( 'tiny_mce_plugins', 'disable_wp_emojis_in_tinymce' );
function disable_wp_emojis_in_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}
/* --------------------------------------------------------------------------- */

/**
 *   Решение
 *   https://wordpress.stackexchange.com/questions/264866/wp-rest-api-for-user-sign-up-registration
 */

function nLbwuEa8_modify_create_user_route() {
	$users_controller = new WP_REST_Users_Controller();

	register_rest_route( 'wp/v2', '/sign-up/user', array(
		array(
			'methods'             => WP_REST_Server::CREATABLE,
			'callback'            => array($users_controller, 'create_item'),
			'permission_callback' => function( $request ) {

				// METHOD 1: Silently force the role to be a subscriber
				$request->set_param('roles', array('subscriber'));

				// METHOD 2: Be nice and provide an error message
				if ( ! current_user_can( 'create_users' ) && $request['roles'] !== array('subscriber')) {

					return new WP_Error(
						'rest_cannot_create_user',
						__( 'Sorry, you are only allowed to create new users with the subscriber role.' ),
						array( 'status' => rest_authorization_required_code() )
					);

				}

				return true;
			},
			'args'                => $users_controller->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
		),
	) );

}
add_action( 'rest_api_init', 'nLbwuEa8_modify_create_user_route' );
